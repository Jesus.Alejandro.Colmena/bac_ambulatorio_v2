# Proyecto Bac Ambulatorio

 Proyecto actualizado con utomatización de pruebas funcionales Bac Ambulatorio.

## Comenzando 🚀

_Para clonar el proyecto en tu máquina local para propósitos de desarrollo y pruebas._

link https://gitlab.com/isapre-colmena-ti/qa-colmena/bac-ambulatorio


### Pre-requisito 📋

_Son necesaria 4 variables de entorno para poder ejecutar cualquier prueba automatizada o regresion del proyecto_

```
EXECUTION   : <Tipo de jeecucion que se realizara esta puede ser local o hub>
URL         : <url bac ambulatorio>
BROWSER     : <navegador en el cual se ejecutaran las pruebas>
```
_Si estas variables no estan creados en la maquina en la cual se ejecutaran las pruebas, el proyecto no iniciara, en el caso de ejecutar en pipeline revisar archivo yml._

## Ejecutando las pruebas ⚙️

_Las pruebas se ejecutan mediante comnados maven, Hay 2 formas de ejecutar pruebas automatizadas_

_Pruebas ondemand ._

```
mvn -Denv=ondemand test -Dcucumber.options="-t @<Scenario a ejecutar>"
```

_Pruebas de Regresion ._
_Estas pruebas estan configuradas por profile en configuracion de archivo pom, para la ejecucion se tomara el profile que este configurado de la siguiente forma: ._
_<activeByDefault>true</activeByDefault> ._


```
mvn clean test
```

### Resultados de pruebas 🔩

_Al finalizar las ejecuciones los resultados de las pruebas estaran disponibles en la carpeta /Reporte la cual contiene los .html de cada flujo y una carpeta /img con las imagenes correspondientes._


## Construido con 🛠️

_Todas las tecnologias utilizadas se encuentran en archivo pom.xml ._

## Autores ✒️


* **Jesús Galvez**   - *QA Automation Jr.* - (jesus.galvez@hakalab.com)
