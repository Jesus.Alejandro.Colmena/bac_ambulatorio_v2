package com.hakalab.archetype.flow.regresion;

import org.junit.runner.RunWith;
import com.hakalab.archetype.flow.BaseFlow;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
	    features = {"src/test/resources/features"}
	    ,glue = "com/hakalab/archetype/definition"
		,tags = {"@Inexistente"}
	    ,plugin = {"com.cucumber.listener.ExtentCucumberFormatter:Reporte/Ondemand.html"}
	    )

public class H1_ingresoCredencialesInvalidasTest extends BaseFlow{

}
