package com.hakalab.archetype.flow.regresion;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import com.cucumber.listener.Reporter;
import com.hakalab.archetype.flow.BaseFlow;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
	    features = {"src/test/resources/features"}
	    ,glue = "com/hakalab/archetype/definition"
		,tags = {"@Consultor"}
	    ,plugin = {"com.cucumber.listener.ExtentCucumberFormatter:Reporte/Ondemand.html"}
	    )

public class H1_ingresoConsultorTest extends BaseFlow {

	@AfterClass
	public static void setUpFinal() throws Exception{
		String rutaXML = System.getProperty("user.dir")+"/src/test/resources/extent-config.xml";
	 	Reporter.loadXMLConfig(rutaXML);
	}
	
}
