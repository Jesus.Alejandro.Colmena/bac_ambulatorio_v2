package com.hakalab.archetype.flow;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.hakalab.archetype.model.PageModel;
import com.ibatis.sqlmap.client.SqlMapClient;

@RunWith(Suite.class)
public class BaseFlow {
	public static PageModel pageModel;
//	public static WebDriver driver;
	public static SqlMapClient sqlMap;
	public static SqlMapClient sqlMapGrafana;
	public static RemoteWebDriver driver;
	public static DesiredCapabilities cap = null;
	public static String sandbox;
	public static String url;

	@BeforeClass
	public static void InitializeWebDriver() throws Exception {
		getEnvironment();
		url = System.getenv("URL").toLowerCase();
		switch (System.getenv("EXECUTION").toUpperCase()) {
		case "LOCAL":
			setDriverSeleniumWD();
			break;
		case "HUB":
			setDriverSeleniumGrid();
			break;
		default:
			break;
		}
 	}

	public static void setDriverSeleniumWD() throws Exception {
        switch (System.getenv("BROWSER").toUpperCase()) {
		case "CHROME":
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+ "/driver/Chrome/" + pathDirDriverChromeOS());
			Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("profile.default_content_setting_values.notifications", 2);
            ChromeOptions options = new ChromeOptions();
            options.setExperimentalOption("prefs", prefs);
            driver = new ChromeDriver(options);
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().timeouts().pageLoadTimeout(180, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            break;
        default:
            break;
        }
    }
	
	public static void setDriverSeleniumGrid() throws IOException {
		 DesiredCapabilities capabilities = DesiredCapabilities.chrome();//se le indica la opcion de navegador que tendra el driver
       	 ChromeOptions chromeOptions=new ChromeOptions();
       	 chromeOptions.addArguments("--no-sandbox");
       	 chromeOptions.addArguments("--disable-dev-shm-usage");
       	 chromeOptions.addArguments("--start-maximized");
       	 chromeOptions.addArguments("--window-size=1920,960");
       	 capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
       	 //driver = new RemoteWebDriver(new URL("http://960846054a8b.ngrok.io/wd/hub"), capabilities);    
       	 //driver = new RemoteWebDriver(new URL("http://d47431751f80.ngrok.io/wd/hub"), capabilities);
       	 //driver = new RemoteWebDriver(new URL("http://192.168.99.100:4446/wd/hub"), capabilities); //selenium Docker      
       	 //driver = new RemoteWebDriver(new URL("tcp://docker:2375/wd/hub"), capabilities); //selenium Docker  
         driver = new RemoteWebDriver(new URL("http://docker:4444/wd/hub"), capabilities);
       	 driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
         driver.manage().timeouts().pageLoadTimeout(500, TimeUnit.SECONDS);

		}
	

	public static String pathDirDriverChromeOS() throws Exception{
		try {
			String os = getOperativeSystem();
			switch (os) {
			case "LINUX32":
				return "Linux/32/chromedriver";
			case "LINUX64":
				return "Linux/64/chromedriver";
			case "WIN":
				return "Windows/chromedriver.exe";
			case "MACOS":
				return "MacOS/chromedriver";
			default:
				return "";
		}
		} catch (IOException e) {
			throw e;
		}
		
	}

	/**
     * Get os from the running machine
     * @return the proper operative system name
     * @throws Exception throws exception if the given OS is not supported.
     */
    public static String getOperativeSystem() throws Exception{
        String osarch;
        String os   = System.getProperty("os.name").toLowerCase();
        String arch = System.getProperty("os.arch");

        if ((os.indexOf("mac") >= 0) || (os.indexOf("darwin") >= 0)) {
            osarch = "MACOS";
        } else if (os.indexOf("win") >= 0) {
            osarch = "WIN";
        } else if (os.indexOf("nux") >= 0) {
            osarch = "LINUX";
        } else {
            osarch = "OTHER";
        }
        if(osarch.equals("LINUX")){
            if(arch.contains("64")){
                osarch = osarch.concat("64");
            } else {
                osarch = osarch.concat("32");
            }
        }
        String regex = "MACOS|WIN|LINUX32|LINUX64";
        System.out.println("The OS is: " + osarch);
        if(!osarch.toUpperCase().matches(regex))
            throw new Exception("Invalid Operative System");
        return osarch;
    }
    
    public static void getEnvironment() throws Exception {
    	 String regexExecution = "LOCAL|HUB";
         System.out.println("The execution is: " + System.getenv("EXECUTION"));
         if(!regexExecution.contains(System.getenv("EXECUTION").toUpperCase())) {
        	 System.out.println("Invalid environment");
             throw new Exception("Invalid environment");
         }
         String regexBrowser = "CHROME";
         System.out.println("The browser is: " + System.getenv("BROWSER"));
         if(!regexBrowser.contains(System.getenv("BROWSER").toUpperCase())) {
        	 System.out.println("Invalid browser");
             throw new Exception("Invalid browser");
         }
         String regexUrl = "http://4.0.13.90:8080/bac/"; 
         System.out.println("The url is: " + System.getenv("URL"));
         if(!regexUrl.contains(System.getenv("URL"))) {
        	 System.out.println("Invalid url");
        	 throw new Exception("Invalid url");
         }
    }
    
	@AfterClass
	public static void setUpFinal() throws Exception {
		driver.quit();
	}
}