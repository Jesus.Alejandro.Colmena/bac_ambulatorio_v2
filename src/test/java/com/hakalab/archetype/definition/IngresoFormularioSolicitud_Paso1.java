package com.hakalab.archetype.definition;

import static org.junit.Assert.assertTrue;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import com.google.common.io.Files;
import com.hakalab.archetype.flow.BaseFlow;
import com.hakalab.archetype.util.TestCaseSingleton;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class IngresoFormularioSolicitud_Paso1 {
	
	 TestCaseSingleton test=TestCaseSingleton.getInstance();
	 String archivoCargadoOK;
	
	@Given("^Me encuentro en Ingreso de formulario Paso(\\d+)$")
	public void me_encuentro_en_Ingreso_de_formulario_Paso(int arg1) throws Throwable {
	   //PENDIENTE LLEGAR A LA CREACIÓN DE LA SOLICITUD Y ID CORRELATIVO
		//FLUJO PARA LLEGAR A PASO1
		try {
			BaseFlow.driver.findElement(By.xpath("//Button[contains(text(),'Crear Solicitud')]")).click();
			BaseFlow.driver.findElement(By.xpath("//Button[contains(text(),'Sí')]")).click();
			BaseFlow.driver.findElement(By.xpath("//Button[contains(text(),'Aceptar')]")).click();		
		} catch (Exception e) {
			assertTrue("Botón crear solicitud no se encuentra habilitado, error de flujo de prueba", false);
		}
	}

	@Then("^verifico si ID de solicitud se genero de forma correlativa$")
	public void verifico_si_ID_de_solicitud_se_genero_de_forma_correlativa() throws Throwable {
		BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Ingreso Fomulario Solicitud Bac(Paso 1)')]")).click();
		test.setTestCaseId("VTDG-16");	
		test.setMensaje("No se encuentra ningún mensaje para indicar al usuario el ingreso de credenciales incorrectas");
		//List<?> ultimoIdSolicitud = DbConnect.getFolioBono();	
		//assertFalse(true);
	}

	@Given("^Se cargue archivo en formato correcto \"([^\"]*)\"$")
	public void se_cargue_archivo_en_formato_correcto(String archivo) throws Throwable {
		//CARGA DE FORMULARIO A 	
		//STRINGSELECTION PARA COPIAR LA RUTA AL DIALOGO DE WINDOWS
		StringSelection stringSelection = new StringSelection(archivo);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		//CLASE ROBOT
		Robot robot = new Robot();	
		//SIN UN TIEMPO DE ESPERA COPIARA LA RUTA MUY RAPIDO Y NO ESPERARA EL DESPLIEGE DE LA VENTANA DE WINDOWS
		Thread.sleep(3000); 
		//CLASE ROBOT KEY PRESS PRESIONA LAS TECLAS INDICADAS
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyPress(KeyEvent.VK_ENTER);
		//CLASE ROBOT KEY RELEASE SUELTA LAS TECLAS O QUEDARAN PRESIONADAS 	
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_ENTER);	
		Thread.sleep(2000); 
		BaseFlow.driver.findElement(By.xpath("//Button[contains(@id,'ext-gen879')][contains(text(),'Agregar/Guardar')]")).click();
	}

	@Then("^Valido que no se pueda adjuntar archivo a solicitud sin descripcion$")
	public void valido_que_no_se_pueda_adjuntar_archivo_a_solicitud_sin_descripcion() throws Throwable {
		test.setTestCaseId("VTDG-17");
		try {
			BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Debe agregar una descripcion mayor a 5 caracteres.')]")).isDisplayed();
			test.setMensaje("PRUEBA CORRECTA: Existe mensaje de alerta en caso de no señalar descripción a archivo a adjuntar.");
			} catch (Exception e) {
			test.setMensaje("PRUEBA FALLIDA: No existe alerta en caso de no señalar ninguna descripción");
			assertTrue("PRUEBA FALLIDA: No existe alerta en caso de no señalar ninguna descripción", false);
			}
	}
	
	@Given("^se cargue archivo con peso superior a maximo permitido \"([^\"]*)\"$")
	public void se_cargue_archivo_con_peso_superior_a_maximo_permitido(String archivo) throws Throwable {
		Thread.sleep(5000); 
		archivoCargadoOK = archivo;
		try {
			BaseFlow.driver.findElements(By.xpath("//div[contains(@id,'x-form-el-documentoGeneral')]")).get(3).click();				
			//CARGA DE FORMULARIO A 	
			//STRINGSELECTION PARA COPIAR LA RUTA AL DIALOGO DE WINDOWS
			StringSelection stringSelection = new StringSelection(archivo);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
			//CLASE ROBOT
			Robot robot = new Robot();	
			//SIN UN TIEMPO DE ESPERA COPIARA LA RUTA MUY RAPIDO Y NO ESPERARA EL DESPLIEGE DE LA VENTANA DE WINDOWS
			Thread.sleep(3000); 
			//CLASE ROBOT KEY PRESS PRESIONA LAS TECLAS INDICADAS
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyPress(KeyEvent.VK_ENTER);
			//CLASE ROBOT KEY RELEASE SUELTA LAS TECLAS O QUEDARAN PRESIONADAS 	
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_ENTER);	
			Thread.sleep(2000); 
			BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'desdocu')]")).sendKeys("DescripcionPrueba");
			BaseFlow.driver.findElement(By.xpath("//Button[contains(@id,'ext-gen879')][contains(text(),'Agregar/Guardar')]")).click();
			//Se pasa valor a variable para validar caso: Validar que sea posible eliminar archivo adjunto.(ultimo caso)
			Thread.sleep(5000); //Tiempo carga de subida archivo
		} catch (Exception e) {
			assertTrue("ERROR: No se pudo adjuntar archivo", false);
		}
	}
	
	@Then("^Valido que aparezca alerta en pantalla\"([^\"]*)\"$")
	public void valido_que_aparezca_alerta_en_pantalla(String pesoArchivo) throws Throwable {
		switch (pesoArchivo) {
		case "superior5mb":
			test.setTestCaseId("VTDG-18");
				try {
					BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'El tamaño del archivo no puede exceder los 5 MB.')]")).isDisplayed();
					test.setMensaje("PRUEBA CORRECTA: Se valida la existencia de mensaje de alerta en caso de que archivo adjunto exceda los 5mb.");
				} catch (Exception e) {
					test.setMensaje("PRUEBA FALLIDA: No se visualiza mensaje de alerta en caso de que archivo ajunto exceda los 5mb.");
					assertTrue(false);
				}
			break;
		case "inferior5mb":
			test.setTestCaseId("VTDG-19");
			try {
				BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'El documento se guardo correctamente.')]")).isDisplayed();
				test.setMensaje("PRUEBA CORRECTA: Archivo con peso inferios a 5mb Guardado y asociado a solicitud correctamente.");
				assertTrue(true);
				//Quitar alerta archivo adjunto
				BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Aceptar')]")).click();
			} catch (Exception e) {
				test.setMensaje("PRUEBA FALLIDA: No se informa que archivo se guardo correctamente.");
				assertTrue(false);
			}
			break;
		default:
			break;
		}
	}

		
	//2 proximos
	@Given("^adjunto archivo de formulario solicitud \"([^\"]*)\"$")
	public void adjunto_archivo_de_formulario_solicitud(String archivo) throws Throwable {
		try {
			if (BaseFlow.driver.findElements(By.xpath("//div[contains(@class,'x-grid3-cell-inner x-grid3-col-eliminar')]")).size() >= 1) {
				Actions action = new Actions(BaseFlow.driver);
				WebElement link = BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-col-eliminar')]"));
				action.doubleClick(link).perform();
				BaseFlow.driver.findElement(By.xpath("//button[contains(@class,'x-btn-text')][contains(text(),'Sí')]")).click();
				BaseFlow.driver.findElements(By.xpath("//div[contains(@id,'x-form-el-documentoGeneral')]")).get(3).click();	
				//CARGA DE FORMULARIO A 	
				//STRINGSELECTION PARA COPIAR LA RUTA AL DIALOGO DE WINDOWS
				StringSelection stringSelection = new StringSelection(archivo);
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
				//CLASE ROBOT
				Robot robot = new Robot();	
				//SIN UN TIEMPO DE ESPERA COPIARA LA RUTA MUY RAPIDO Y NO ESPERARA EL DESPLIEGE DE LA VENTANA DE WINDOWS
				Thread.sleep(3000); 
				//CLASE ROBOT KEY PRESS PRESIONA LAS TECLAS INDICADAS
				robot.keyPress(KeyEvent.VK_CONTROL);
				robot.keyPress(KeyEvent.VK_V);
				robot.keyPress(KeyEvent.VK_ENTER);
				//CLASE ROBOT KEY RELEASE SUELTA LAS TECLAS O QUEDARAN PRESIONADAS 	
				robot.keyRelease(KeyEvent.VK_CONTROL);
				robot.keyRelease(KeyEvent.VK_V);
				robot.keyRelease(KeyEvent.VK_ENTER);	
				Thread.sleep(2000); 
				BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'desdocu')]")).clear();
				BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'desdocu')]")).sendKeys("PruebaExtension");
				BaseFlow.driver.findElement(By.xpath("//Button[contains(@id,'ext-gen879')][contains(text(),'Agregar/Guardar')]")).click();
				Thread.sleep(5000); //Tiempo carga de subida archivo
			} else {
				StringSelection stringSelection = new StringSelection(archivo);
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
				//CLASE ROBOT
				Robot robot = new Robot();	
				//SIN UN TIEMPO DE ESPERA COPIARA LA RUTA MUY RAPIDO Y NO ESPERARA EL DESPLIEGE DE LA VENTANA DE WINDOWS
				Thread.sleep(3000); 
				//CLASE ROBOT KEY PRESS PRESIONA LAS TECLAS INDICADAS
				robot.keyPress(KeyEvent.VK_CONTROL);
				robot.keyPress(KeyEvent.VK_V);
				robot.keyPress(KeyEvent.VK_ENTER);
				//CLASE ROBOT KEY RELEASE SUELTA LAS TECLAS O QUEDARAN PRESIONADAS 	
				robot.keyRelease(KeyEvent.VK_CONTROL);
				robot.keyRelease(KeyEvent.VK_V);
				robot.keyRelease(KeyEvent.VK_ENTER);	
				Thread.sleep(2000); 
				BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'desdocu')]")).clear();
				BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'desdocu')]")).sendKeys("PruebaExtension");
				BaseFlow.driver.findElement(By.xpath("//Button[contains(@id,'ext-gen879')][contains(text(),'Agregar/Guardar')]")).click();
			}
		} catch (Exception e) {
			assertTrue("Error al eliminar el anterior",false);
		}
	}

	@Then("^Valido que se adjunto archivo en formato correcto \"([^\"]*)\"$")
	public void valido_que_se_adjunto_archivo_en_formato_correcto(String archivo) throws Throwable {
		String extension = Files.getFileExtension(archivo);
		switch (extension) {
		case "docx":
			test.setTestCaseId("VTDG-20");
			try {
				BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Formato invalido. Los Siguientes Formatos son Aceptados:PDF,JPG,TXT')]")).isDisplayed();
				test.setMensaje("PRUEBA CORRECTA: no se permite adjuntar archivos en formato: WORD(docx)");
			} catch (Exception e) {
				test.setMensaje("PRUEBA FALLIDA: no existe mensaje de alerta al intentar adjuntar archivo en formato: Word(docx)");
				assertTrue(false);
			}
			break;
        case "xlsx":
        	test.setTestCaseId("VTDG-21");
        	try {
				BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Formato invalido. Los Siguientes Formatos son Aceptados:PDF,JPG,TXT')]")).isDisplayed();
				test.setMensaje("PRUEBA CORRECTA: no se permite adjuntar archivos en formato: EXCEL(xlsx)");
			} catch (Exception e) {
				test.setMensaje("PRUEBA FALLIDA: no existe mensaje de alerta al intentar adjuntar archivo en formato: EXCEL(xlsx)");
				assertTrue(false);
			}
			break;
        case "pdf":
        	test.setTestCaseId("VTDG-22");
        	try {
				BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'El documento se guardo correctamente.')]")).isDisplayed();
				test.setMensaje("PRUEBA CORRECTA: archivo formato: PDF adjuntado corretamente");
				BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Aceptar')]")).click();
			} catch (Exception e) {
				test.setMensaje("PRUEBA FALLIDA: No existe mensaje informando que archivo se adjunto correctamente");
				assertTrue(false);
			}
			break;
        case "jpg":
        	test.setTestCaseId("VTDG-23");
			try {
				BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'El documento se guardo correctamente.')]")).isDisplayed();
				test.setMensaje("PRUEBA CORRECTA: archivo formato: JPG adjuntado corretamente");
				BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Aceptar')]")).click();
			} catch (Exception e) {
				test.setMensaje("PRUEBA FALLIDA: No existe mensaje informando que archivo se adjunto correctamente");
				assertTrue(false);
				BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Aceptar')]")).click();
			}
			break;
        case "txt":
        	test.setTestCaseId("VTDG-24");
        	try {
        		BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'El documento se guardo correctamente.')]")).isDisplayed();
        		test.setMensaje("PRUEBA CORRECTA: archivo formato: TXT adjuntado corretamente");
        		BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Aceptar')]")).click();
        		assertTrue(true);
			} catch (Exception e) {
				test.setMensaje("PRUEBA FALLIDA: No existe mensaje informando que archivo se adjunto correctamente");
				assertTrue(false);
			}
			break;
		default:
			assertTrue("Flujo no controlado", false);
			break;
		}
	}

	
	
	@Given("^Exista cargado un formulario de solicitud$")
	public void exista_cargado_un_formulario_de_solicitud() throws Throwable {
		try {
		BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-cell-inner x-grid3-col-eliminar')]")).isDisplayed();
		} catch (Exception e) {
		assertTrue("ERROR NO CONTROLADO 1",false);
		}
	}

	@Then("^Validar que no se pueda adjuntar mas de un formulario\"([^\"]*)\"$")
	public void validar_que_no_se_pueda_adjuntar_mas_de_un_formulario(String archivo) throws Throwable {
		test.setTestCaseId("VTDG-25");
		try {
			BaseFlow.driver.findElements(By.xpath("//button[contains(@id,'')][contains(text(),'Buscar Archivo')]")).get(0);	
			//CARGA DE FORMULARIO A 	
			//STRINGSELECTION PARA COPIAR LA RUTA AL DIALOGO DE WINDOWS
			StringSelection stringSelection = new StringSelection(archivo);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
			//CLASE ROBOT
			Robot robot = new Robot();	
			//SIN UN TIEMPO DE ESPERA COPIARA LA RUTA MUY RAPIDO Y NO ESPERARA EL DESPLIEGE DE LA VENTANA DE WINDOWS
			Thread.sleep(3000); 
			//CLASE ROBOT KEY PRESS PRESIONA LAS TECLAS INDICADAS
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyPress(KeyEvent.VK_ENTER);
			//CLASE ROBOT KEY RELEASE SUELTA LAS TECLAS O QUEDARAN PRESIONADAS 	
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_ENTER);	
			Thread.sleep(2000); 
			BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'desdocu')]")).sendKeys("DescripcionPrueba");
			BaseFlow.driver.findElement(By.xpath("//Button[contains(@id,'ext-gen879')][contains(text(),'Agregar/Guardar')]")).click();
			Thread.sleep(5000); //Tiempo carga de subida archivo
			//BaseFlow.driver.findElement(By.xpath("")).isDisplayed(); verificar si se agrega mensaje al agregar archivo
			test.setMensaje("PRUEBA CORRECTA: No se puede adjuntar mas de un formulario BAC");
			assertTrue(true);
		} catch (Exception e) {
			test.setMensaje("PRUEBA FALLIDA: No existe mensaje al intentar adjuntar otro formulario bac: solo se permite 1");
			assertTrue(false);
		}
	}


	@Given("^Exsta cargado un formulario de solicitud$")
	public void exsta_cargado_un_formulario_de_solicitud() throws Throwable {
		try {
			BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-cell-inner x-grid3-col-eliminar')]")).isDisplayed();
			} catch (Exception e) {
				try {
					BaseFlow.driver.findElements(By.xpath("//button[contains(@id,'')][contains(text(),'Buscar Archivo')]")).get(0);	
					//CARGA DE FORMULARIO A 	
					//STRINGSELECTION PARA COPIAR LA RUTA AL DIALOGO DE WINDOWS
					StringSelection stringSelection = new StringSelection(archivoCargadoOK);
					Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
					//CLASE ROBOT
					Robot robot = new Robot();	
					//SIN UN TIEMPO DE ESPERA COPIARA LA RUTA MUY RAPIDO Y NO ESPERARA EL DESPLIEGE DE LA VENTANA DE WINDOWS
					Thread.sleep(3000); 
					//CLASE ROBOT KEY PRESS PRESIONA LAS TECLAS INDICADAS
					robot.keyPress(KeyEvent.VK_CONTROL);
					robot.keyPress(KeyEvent.VK_V);
					robot.keyPress(KeyEvent.VK_ENTER);
					//CLASE ROBOT KEY RELEASE SUELTA LAS TECLAS O QUEDARAN PRESIONADAS 	
					robot.keyRelease(KeyEvent.VK_CONTROL);
					robot.keyRelease(KeyEvent.VK_V);
					robot.keyRelease(KeyEvent.VK_ENTER);	
					Thread.sleep(2000); 
					BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'desdocu')]")).sendKeys("DescripcionPrueba");
					BaseFlow.driver.findElement(By.xpath("//Button[contains(@id,'ext-gen879')][contains(text(),'Agregar/Guardar')]")).click();
					Thread.sleep(5000); //Tiempo carga de subida archivo
					assertTrue(true);
				} catch (Exception e1) {
					assertTrue(false);
				}		
			}
	}

	@When("^Intento eliminar formulario$")
	public void intento_eliminar_formulario() throws Throwable {
		try {
			Actions action = new Actions(BaseFlow.driver);
			WebElement link = BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-col-eliminar')]"));
			action.doubleClick(link).perform();
			//Validar si existe mensaje
			BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Esta Seguro De Elimar El Documento Solicitud?')]")).isDisplayed();
			BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Sí')]")).click();
		} catch (Exception e) {
			assertTrue("Error al eliminar formulario adjunto",false);
		}
	}

	@Then("^Valido que forulario se elimino correctamente$")
	public void valido_que_forulario_se_elimino_correctamente() throws Throwable {
		test.setTestCaseId("VTDG-26");
		try {
		BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-cell-inner x-grid3-col-eliminar')]")).isDisplayed();
		test.setMensaje("PRUEBA FALLIDA: No se elimino formulario BAC: SOLO SE PERMITE 1");
		} catch (Exception e) {
			test.setMensaje("PRUEBA CORRECTA: Se elimino formulario correctamente");
			assertTrue(true);
		}
	}
	
}
