package com.hakalab.archetype.definition;

import static org.junit.Assert.assertTrue;
import org.openqa.selenium.By;
import com.hakalab.archetype.flow.BaseFlow;
import com.hakalab.archetype.util.GenericMethods;
import com.hakalab.archetype.util.TestCaseSingleton;
import com.hakalab.archetype.util.TestLink;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class IngresoBacAmbulatorio {
	
	   //INFORME TESTLINK
     TestCaseSingleton test=TestCaseSingleton.getInstance();
	@Before
	    public void updateTest() {
	         test.setTestCaseId(null);
	            test.setMensaje(null);
	    }
	    
	   @After // tag que indica que se ejcutara antes de cada metodo
	   public void loadImage(Scenario scenario) throws Exception {// metodo que escribira log al final de cada scenario
	     if (test.getTestCaseId()!=null) {
	         TestLink.updateTestCase(test.getTestCaseId(), test.getMensaje(), scenario);//metodo para actualizar testlink
	    }
	           GenericMethods.screenShotForScenarioFailed(scenario);// metodo que captura screenshot en caso de pasos fallidos
	   }
		

	@Given("^me encuentro en ruta BacAmbulatorio$")
	public void me_encuentro_en_ruta_BacAmbulatorio() throws Throwable {
		//BaseFlow.driver.navigate().to("http://4.0.13.90:8080/bac/");
		//BaseFlow.driver.get(BaseFlow.url);
		BaseFlow.driver.navigate().to("http://4.0.13.90:8080/bac/");
	}

	@When("^ingreso usuario y contraseña \"([^\"]*)\"\"([^\"]*)\"$")
	public void ingreso_usuario_y_contraseña(String usuario, String contraseña) throws Throwable {
	 BaseFlow.driver.findElement(By.xpath("//input[contains(@name,'username')]")).sendKeys(usuario);
	 BaseFlow.driver.findElement(By.xpath("//input[contains(@name,'password')]")).sendKeys(contraseña);
	 BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Login')]")).click();	 
	}

	@Then("^Valido que se ingreso con exito con perfil \"([^\"]*)\"$")
	public void valido_que_se_ingreso_con_exito_con_perfil(String tipoUsuario) throws Throwable {
		try {
			BaseFlow.driver.findElement(By.xpath("//*[contains(text(),'Bienvenido(a):')]")).isDisplayed();
			assertTrue("Ingreso Correcto", true);	
		} catch (Exception e) {
			test.setTestCaseId("VTDG-9");			try {
				BaseFlow.driver.findElement(By.xpath("//*[contains(text(),'El Usuario no existe')]")).isDisplayed();
				test.setMensaje("Se valida que existe mensaje indicando que el usuario no existe");
			} catch (Exception e2) {
				test.setMensaje("No se encuentra ningún mensaje para indicar al usuario el ingreso de credenciales incorrectas");
			}
		}
	}
	

	@Then("^Valido menus habilitados para usuario \"([^\"]*)\"$")
	public void valido_menus_habilitados_para_usuario(String tipoUsuario) throws Throwable {
		switch (tipoUsuario) {
		case "consultor":
			test.setTestCaseId("VTDG-6");
			try {
				BaseFlow.driver.findElement(By.xpath("//table[contains(@id,'ext-comp-1008') and contains(@class,'x-item-disabled')]")).isDisplayed();
				BaseFlow.driver.findElement(By.xpath("//button[contains(@id,'ext-gen29')]")).click();
				BaseFlow.driver.findElement(By.xpath("//li[contains(@id,'ext-gen67') and contains(@class,'x-item-disabled')]")).isDisplayed();
				test.setMensaje("Se valida que CONSULTOR no tiene acceso a los módulos Solicitud Reembolso y BackOffice");
			} catch (Exception e) {
				test.setMensaje("CONSULTOR posee accesos de creación de casos");
			}
		break;
		case "creador":
			test.setTestCaseId("VTDG-8");
			try {
				BaseFlow.driver.findElement(By.xpath("//table[contains(@id,'ext-comp-1008') and contains(@class,'x-btn-wrap x-btn x-btn-text-icon')]")).isDisplayed();
				BaseFlow.driver.findElement(By.xpath("//button[contains(@id,'ext-gen29')]")).click();
				BaseFlow.driver.findElement(By.xpath("//li[contains(@id,'ext-gen67') and contains(@class,'x-menu-list-item')]")).isDisplayed();
				test.setMensaje("Perfil CREADOR posee los médulos/opciones necesarias.");
			} catch (Exception e) {
				test.setMensaje("Perfil CREADOR no posee uno de los médulos/opciones necesarias.");
			}
		break;
		default:
		break;
		}
	}
	
}
