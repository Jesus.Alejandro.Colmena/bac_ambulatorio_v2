package com.hakalab.archetype.definition;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;

import com.hakalab.archetype.flow.BaseFlow;
import com.hakalab.archetype.util.BacMethods;
import com.hakalab.archetype.util.GenericMethods;
import com.hakalab.archetype.util.TestCaseSingleton;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class IngresoBitacora_Paso4 {
	
	//INFORME TESTLINK
    TestCaseSingleton test=TestCaseSingleton.getInstance();

	@Given("^me encuentro en sección bitácora$")
	public void me_encuentro_en_sección_bitácora() throws Throwable {
	  BacMethods.crearSolicitudBac();
	  BaseFlow.driver.findElement(By.xpath("//Span[contains(text(),'Bitacora')]")).click();
	}

	@Then("^valido que exista primera bitácora creada$")
	public void valido_que_exista_primera_bitácora_creada() throws Throwable {
		test.setTestCaseId("VTDG-39");
		if (GenericMethods.existElement(By.xpath("//Div[contains(@class,'x-grid3-col-bac_globserv')][contains(text(),'Creacion de Solicitud')]"))) {
			test.setMensaje("PRUEBA CORRECTO: Se valida existencia de primer registro de modulo de bitácora.");
		} else {
			test.setMensaje("PRUEBA FALLIDA: No se encuentra primer regisyro de bitacora.");
		}
	}

	
	@When("^agrego bitácora nueva sin glosa$")
	public void agrego_bitácora_nueva_sin_glosa() throws Throwable {
		try {
			BaseFlow.driver.findElement(By.xpath("//button[contains(@class,'icon-add')][contains(text(),'Agregar')]")).click();
			BaseFlow.driver.findElement(By.xpath("//textarea[contains(@id,'glosaBitacora')]")).click();
			BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Grabar')]")).click();
			BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Sí')]")).click();
			//BaseFlow.driver.findElement(By.xpath("//textarea[contains(@id,'glosaBitacora')]")).sendKeys("");
			assertTrue(true);
		} catch (Exception e) {
			assertTrue("ERROR: NO SE ENCUENTRAN LOS ELEMENTOS WEB PARA GREGAR GLOSA",true);
		}
	}

	@Then("^valido que exista mensaje de alerta$")
	public void valido_que_exista_mensaje_de_alerta() throws Throwable {
		test.setTestCaseId("VTDG-41");
		if (GenericMethods.existElement(By.xpath("//span[contains(text(),'Debe ingresar Glosa')]"))) {
			test.setMensaje("PRUEBA CORRECTA: Existe mensaje de alerta indicando que se debe ingresar Glosa.");
			assertTrue("OK",true);
		} else {
			test.setMensaje("PRUEBA FALLIDA: No se alerta en caso de intentar ingresar glosa vacia");
			assertTrue("ERROR",false);
		}
		BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Aceptar')]")).click();
	}
	
	
	@Given("^boton agregar bitacora esta habilitado$")
	public void boton_agregar_bitacora_esta_habilitado() throws Throwable {
		try {
			if (GenericMethods.existElement(By.xpath("//textarea[contains(@id,'glosaBitacora')]"))) {
			} else {
				BaseFlow.driver.findElement(By.xpath("//table[contains(@id,'btnAgregaDocumento1')]//button[contains(text(),'Agregar')]")).click();
			}
		} catch (Exception e) {
			assertTrue("ERROR AL AGREGAR BITACORA OK", false);
		}
		
	}

	@When("^agrego bitacora en formato correcto$")
	public void agrego_bitacora_en_formato_correcto() throws Throwable {
		BaseFlow.driver.findElement(By.xpath("//textarea[contains(@id,'glosaBitacora')]")).sendKeys("GlosaOK");	
		BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Grabar')]")).click();
		BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Sí')]")).click();
	}

	@Then("^valido que se agrego bitacora correctamente$")
	public void valido_que_se_agrego_bitacora_correctamente() throws Throwable {
		test.setTestCaseId("VTDG-40");
		if (GenericMethods.existElement(By.xpath("//span[contains(text(),'Ingreso Completado')]"))) {
			test.setMensaje("PRUEBA CORRECTA: Existe mensaje confirmando el ingreso.");
			assertTrue(true);
		} else {
			test.setMensaje("PRUEBA Fallida: No eiste mensaje confirmando el ingreso de una nueva bitácora.");
			assertTrue("NO EXISTE MENSAJE CONFIRMANDO INGRESO BITÁCORA",false);
		}
	}

	
	

	@Given("^me encuentro en ingreso de glosa bitacora$")
	public void me_encuentro_en_ingreso_de_glosa_bitacora() throws Throwable {
		if (GenericMethods.existElement(By.xpath("//button[contains(text(),'Aceptar')]"))) {
			BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Aceptar')]")).click();
			BaseFlow.driver.findElement(By.xpath("//button[contains(@class,'icon-add')][contains(text(),'Agregar')]")).click();
			assertTrue(true);
		} else {
			BaseFlow.driver.findElement(By.xpath("//button[contains(@class,'icon-add')][contains(text(),'Agregar')]")).click();
			assertTrue(true);
		}
	}

	@When("^ingreso bitacora con glosa Extensa \"([^\"]*)\"$")
	public void ingreso_bitacora_con_glosa_Extensa(String glosaExtensa) throws Throwable {
		BaseFlow.driver.findElement(By.xpath("//textarea[contains(@id,'glosaBitacora')]")).sendKeys(glosaExtensa);
		BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Grabar')]")).click();
	}

	@Then("^valido que ingreso se realice sin problemas$")
	public void valido_que_ingreso_se_realice_sin_problemas() throws Throwable {
		test.setTestCaseId("VTDG-42");
		if (GenericMethods.existElement(By.xpath("//span[contains(text(),'Solo puede ingresar 300 caracteres como glosa.')]"))) {
			test.setMensaje("PRUEBA CORRECTA: Se informa mensaje de alerta en pantalla.");
		}else {
			test.setMensaje("PRUEBA FALLIDA: No se controla el ingreso de glosa extensa.");
		}
	}
	
	
	
}
