package com.hakalab.archetype.definition;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;

import com.hakalab.archetype.flow.BaseFlow;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.hakalab.archetype.util.BacMethods;
import com.hakalab.archetype.util.GenericMethods;
import com.hakalab.archetype.util.IngresoGasto;
import com.hakalab.archetype.util.TestCaseSingleton;

public class IngresoGasto_Paso2 {
	
	//Variable para enviar actualizacion de estado según correspondan tipos de formatos VALIDOS- INVALIDOS
	String caseIdFormato;	
	//INFORME TESTLINK
    TestCaseSingleton test=TestCaseSingleton.getInstance();

	@Given("^Valido que me encuentro en Paso(\\d+)$")
	public void valido_que_me_encuentro_en_Paso(int arg1) throws Throwable {
	  IngresoGasto.meEncuentroEnPaso(arg1);
	}

	@When("^adjunto archivo de gasto sin descripción$")
	public void adjunto_archivo_de_gasto_sin_descripción() throws Throwable {
		//Enviar string gasto para adjuntar archivo ok de gasto
		BaseFlow.driver.findElement(By.xpath("//div[contains(@id,'x-form-el-documentoGeneral1')]")).click();		
	    BacMethods.adjuntarArchivo("gasto");
	    BaseFlow.driver.findElement(By.xpath("//table[contains(@id,'btnAgregaDocumento1')]")).click();
	}

	@Then("^Valido que no se pueda agregar gasto sin descripción$")
	public void valido_que_no_se_pueda_agregar_gasto_sin_descripción() throws Throwable {
		test.setTestCaseId("VTDG-27");
		try {
			BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Debe agregar una descripcion mayor a 5 caracteres')]")).isDisplayed();
			test.setMensaje("PRUEBA CORRECTA: Existe validación indicando caracteristicas que debe cumplir texto ingresado en campo DESCRIPCIÓN.");
		} catch (Exception e) {
			test.setMensaje("PRUEBA FALLIDA: No existe validación al intentar dejar desccripción vacia");
		}
	}

		
	@Given("^Existe texto en campo informacion$")
	public void existe_texto_en_campo_informacion() throws Throwable {
	   BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).clear();
	   BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).sendKeys("GastoPesoSuperior5mb");
	}

	@When("^adjunto archivo de gasto con peso superior al permitido$")
	public void adjunto_archivo_de_gasto_con_peso_superior_al_permitido() throws Throwable {
		assertTrue("Error", IngresoGasto.adjuntarArchivo("gastoSuperior5mb"));
	}

	@Then("^valido que no se pueda agregar archivo de gasto$")
	public void valido_que_no_se_pueda_agregar_archivo_de_gasto() throws Throwable {
		test.setTestCaseId("VTDG-28");
		try {
			BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'El tamaño del archivo no puede exceder los 5 MB.')]")).isDisplayed();
			test.setMensaje("PRUEBA CORRECTA: Existe mensaje validando  que no se puedan adjuntar archivos que excedan peso límite.");
		} catch (Exception e) {
			test.setMensaje("PRUEBA FALLIDA: No existe mensaje de validación en caso de adjuntar archivo con peso superior a 5mb. ");
		}
	}
	
	
	//VALIDACION DE FORMATOS NO PERMITIDOS
	@Given("^Existe texto en campo información$")
	public void existe_texto_en_campo_información() throws Throwable {
		BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).clear();
		BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).sendKeys("ExtensionesNoPermitidas");
	}

	@When("^adjunto archivo en formato no permitido \"([^\"]*)\"$")
	public void adjunto_archivo_en_formato_no_permitido(String archivo) throws Throwable {
		caseIdFormato = archivo;
		assertTrue("Error", IngresoGasto.adjuntarArchivo(archivo));
	}
	
	@Then("^valido que aparezca alerta en pantalla$")
	public void valido_que_aparezca_alerta_en_pantalla() throws Throwable {
		//test.setTestCaseId("VTDG-");		
			switch (caseIdFormato) {
			case "word":
				test.setTestCaseId("VTDG-29");
				try {				
					BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Formato invalido. Los Siguientes Formatos son Aceptados:PDF,JPG,TXT')]")).isDisplayed();
					test.setMensaje("PRUEBA CORRECTA: Se visualiza alerta en caso de adjuntar archivos con extensiones no permitidas");			
				} catch (Exception e) {
					test.setMensaje("PRUEBA FALLIDA: No se visualiza NINGUNA alerta en caso de adjuntar archivos con extensiones no permitidas");
					assertTrue(false);
				}
				break;
			case "excel":
				test.setTestCaseId("VTDG-30");
				try {				
					BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Formato invalido. Los Siguientes Formatos son Aceptados:PDF,JPG,TXT')]")).isDisplayed();
					test.setMensaje("PRUEBA CORRECTA: Se visualiza alerta en caso de adjuntar archivos con extensiones no permitidas");			
				} catch (Exception e) {
					test.setMensaje("PRUEBA FALLIDA: No se visualiza NINGUNA alerta en caso de adjuntar archivos con extensiones no permitidas");
					assertTrue(false);
				}
				break;
			case "xml":
				test.setTestCaseId("VTDG-31");
				try {				
					BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Formato invalido. Los Siguientes Formatos son Aceptados:PDF,JPG,TXT')]")).isDisplayed();
					test.setMensaje("PRUEBA CORRECTA: Se visualiza alerta en caso de adjuntar archivos con extensiones no permitidas");			
				} catch (Exception e) {
					test.setMensaje("PRUEBA FALLIDA: No se visualiza NINGUNA alerta en caso de adjuntar archivos con extensiones no permitidas");
					assertTrue(false);
				}
				break;
			default:
				assertTrue("No se encontro validación para tipo de extensión", false);
				break;
			}
	
	}
	
	
	//VALIDACION DE FORMATOS PERMITIDOS
	@Given("^Existe texto en campo informacionArchivo$")
	public void existe_texto_en_campo_informacionArchivo() throws Throwable {
		BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).clear();
		BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).sendKeys("ExtensionesNoPermitidas");
	}

	@When("^adjunto archivo en formato correcto \"([^\"]*)\"$")
	public void adjunto_archivo_en_formato_correcto(String archivo) throws Throwable {
		caseIdFormato = archivo;
		assertTrue("Error", IngresoGasto.adjuntarArchivo(archivo));   
	}

	@Then("^valido que archivo se encuentra adjunto$")
	public void valido_que_archivo_se_encuentra_adjunto() throws Throwable {
		switch (caseIdFormato) {
		case "pdf":
			test.setTestCaseId("VTDG-32");
			try {				
				BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Documento grabado correctamente.')]")).isDisplayed();
				test.setMensaje("PRUEBA CORRECTA: Se adjunto correctamente archivo en extension .DOCX");			
			} catch (Exception e) {
				test.setMensaje("PRUEBA FALLIDA: No se visualiza mensaje confirmando que se grabó correctamente el documento");
				assertTrue(false);
			}
			break;
		case "txt":
			test.setTestCaseId("VTDG-33");
			try {				
				BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Documento grabado correctamente.')]")).isDisplayed();
				test.setMensaje("PRUEBA CORRECTA: Se adjunto correctamente archivo en extension .XLS");			
			} catch (Exception e) {
				test.setMensaje("PRUEBA FALLIDA: No se visualiza mensaje confirmando que se grabó correctamente el documento");
				assertTrue(false);
			}
			break;
		case "jpeg":
			test.setTestCaseId("VTDG-34");
			try {				
				BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Documento grabado correctamente.')]")).isDisplayed();
				test.setMensaje("PRUEBA CORRECTA: Se adjunto correctamente archivo en extension .XML");			
			} catch (Exception e) {
				test.setMensaje("PRUEBA FALLIDA: No se visualiza mensaje confirmando que se grabó correctamente el documento");
				assertTrue(false);
			}
			break;
		default:
			assertTrue("No se encontro validación para tipo de extensión", false);
			break;
		}
	}

	
	//VALIDAR QUE NO SE PUEDA ADJUNTAR UN MISMO ARCHIVO REPETIDO
	@Given("^Existe descripcion en campo descripcionDeGasto$")
	public void existe_descripcion_en_campo_descripcionDeGasto() throws Throwable {
		BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).clear();
		BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).sendKeys("ArchivoRepetido");
	}

	@Given("^no existe ningún archivo adjunto$")
	public void no_existe_ningún_archivo_adjunto() throws Throwable {
		try {
			BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Aceptar')]")).click();
		} finally {
			assertTrue("ERROR AL LIMPIAR TABLA", IngresoGasto.limpiarTabla());
		}
	}

	@When("^adjunto un mismo archivo dos veces$")
	public void adjunto_un_mismo_archivo_dos_veces() throws Throwable {
		assertTrue("Error", IngresoGasto.adjuntarArchivo("gasto")); 
		Thread.sleep(5000);
		assertTrue("Error", IngresoGasto.adjuntarArchivo("gasto"));  
	}

	@Then("^Valido que no se pueda adjuntar un mismo archivo$")
	public void valido_que_no_se_pueda_adjuntar_un_mismo_archivo() throws Throwable {
		test.setTestCaseId("VTDG-35");
		if (GenericMethods.existElement(By.xpath("//span[contains(text(),'Documento ya existe, revise nombre.')]"))) {
			test.setMensaje("PRUEBA CORRECTA: No se pueden adjuntar archivos con el mismo nombre");
		}else {
			test.setMensaje("PRUEBA FALLIDA: No existe ninguna alerta al momento de intentar adjuntar archivos con el mismo nombre ");
			assertTrue(false);
		}
	}

	
	//VALIDAR QUE SE PUEDA ADJUNTAR MAS DE UN ARCHIVO PERMITIDO
	@Given("^Existe descripcion masDeUnArchivo en campo descripcion$")
	public void existe_descripcion_masDeUnArchivo_en_campo_descripcion() throws Throwable {
		BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).clear();
		BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).sendKeys("MasDeUnArchivoPermitido");
	}

	@Given("^no existe ningun archivo adjunto$")
	public void no_existe_ningun_archivo_adjunto() throws Throwable {
		try {
			BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Aceptar')]")).click();
		} finally {
			assertTrue("ERROR AL LIMPIAR TABLA", IngresoGasto.limpiarTabla());
		}
	}

	@When("^adjunto mas de un archivo permitido$")
	public void adjunto_mas_de_un_archivo_permitido() throws Throwable {
		assertTrue("Error", IngresoGasto.adjuntarArchivo("gasto"));
		Thread.sleep(5000);
		assertTrue("Error", IngresoGasto.adjuntarArchivo("jpeg")); 
	}

	@Then("^valido que exista mas de un archivo en tabla$")
	public void valido_que_exista_mas_de_un_archivo_en_tabla() throws Throwable {
		test.setTestCaseId("VTDG-36");
		BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Aceptar')]")).click();
		try {
			int cantidadRegistros = BaseFlow.driver.findElements(By.xpath("//div[contains(@class,'x-grid3-col-eliminar')]")).size();
			if (cantidadRegistros > 0) {
				test.setMensaje("PRUEBA CORRECTA: Se adjunto correctamente más de un archivo de gasto.");
			}else {
				test.setMensaje("PRUEBA FALLIDA: No se pudo adjuntar mas de un archivo permitido");
				assertTrue(false);
			}
		} catch (Exception e) {
			assertTrue("Error al buscar elementos",false);
		}
	}
	
	
	//VALIDAR PRESTACION INDICADA
	@Given("^Existe descripcion de prestacion en descripción$")
	public void existe_descripcion_de_prestacion_en_descripción() throws Throwable {
		BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).clear();
	}
	
	@Given("^no existen ningun archivo adjunto$")
	public void no_existen_ningun_archivo_adjunto() throws Throwable {
	/*	try {
			BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Aceptar')]")).click();
		} finally { */
			assertTrue("ERROR AL LIMPIAR TABLA", IngresoGasto.limpiarTabla());
		//}
	}

	@When("^adjunto archivo indicando prestacion \"([^\"]*)\"$")
	public void adjunto_archivo_indicando_prestacion(String prestacion) throws Throwable {
		BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).sendKeys("SeIndica:"+ prestacion);
		switch (prestacion) {
		case "sinCobertura":
			BaseFlow.driver.findElement(By.xpath("//label[contains(@for,'bac_sincobn')][contains(text(),'Sin Cobertura')]")).click();
			break;
		case "noArancelada":
			BaseFlow.driver.findElement(By.xpath("//label[contains(@for,'bac_noaran')][contains(text(),'No Arancelada')]")).click();
			break;
		case "otraCobertura":
			BaseFlow.driver.findElement(By.xpath("//label[contains(@for,'bac_otracobertura')][contains(text(),'Otra Cobertura')]")).click();
			break;
		default:
			break;
		}
		IngresoGasto.adjuntarArchivo("gasto");
	}
	
	@Then("^Valido que se indique en tabla prestacion indicada \"([^\"]*)\"$")
	public void valido_que_se_indique_en_tabla_prestacion_indicada(String prestacion) throws Throwable {
		test.setTestCaseId("VTDG-37");
		try {
			BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Aceptar')]")).click();
		} finally {
			boolean resultadoPrestacion = IngresoGasto.ValidarCobertura(prestacion);
			if (resultadoPrestacion) {
				test.setMensaje("PRUEBA CORRECTA: arhivo adjunto posee prestacion indicada de forma correcta");
			} else {
				test.setMensaje("PRUEBA FALLIDA: No se indica prestacion segun los datos ingresados");
				assertTrue(false);
			} 
		}
		
	}
	
	
	
	//VALIDAR ELIMINACION DE ARCHIVO DE GASTO
	@Given("^Existe adjunto en tabla de gastos$")
	public void existe_adjunto_en_tabla_de_gastos() throws Throwable {
		try {
			BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).clear();
			BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).sendKeys("PruebaEliminarRegistro");
		int cantidadAdjuntos = BaseFlow.driver.findElements(By.xpath("//div[contains(@class,'x-grid3-cell-inner x-grid3-col-eliminar')]")).size();
			if (cantidadAdjuntos > 0) {
				assertTrue(true);
			} else {
				IngresoGasto.adjuntarArchivo("gasto");
			}
		} catch (Exception e) {
		assertTrue("ERROR AL CONTAR REGISTROS EN TABLA", false);
		}
	}

	@When("^Elimino archivo de gasto adjunto$")
	public void elimino_archivo_de_gasto_adjunto() throws Throwable {
	   IngresoGasto.limpiarTabla();
	}

	@Then("^Valido que registro de elimino correctamente$")
	public void valido_que_registro_de_elimino_correctamente() throws Throwable {
		test.setTestCaseId("VTDG-38");
	    try {
	    	int cantidadAdjuntos = BaseFlow.driver.findElements(By.xpath("//div[contains(@class,'x-grid3-cell-inner x-grid3-col-eliminar')]")).size();
	    	if (cantidadAdjuntos == 0) {
	    		test.setMensaje("PRUEBA CORRECTA: Archivo eliminado.");	
	    		assertTrue(true);
			}else {
				test.setMensaje("PRUEBA FALLIDA: No se pudo eliminar archivo");
				assertTrue(false);
			}
		} catch (Exception e) {
			assertTrue("ERROR AL CONTAR REGISTROS EN TABLA",false);
		}
	}
	
}
