package com.hakalab.archetype.definition;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.hakalab.archetype.flow.BaseFlow;
import com.hakalab.archetype.util.TestCaseSingleton;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.sourceforge.htmlunit.corejs.javascript.ast.SwitchCase;

public class ConsultarBacBeneficiario {
	
	//ACTUALIZACIÓN TESTLINK
	TestCaseSingleton test=TestCaseSingleton.getInstance();

	@Given("^Me encuentro en Consulta-Rut Beneficiario$")
	public void me_encuentro_en_Consulta_Rut_Beneficiario() throws Throwable {
		try {
			BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Consulta - Rut Beneficiario')]")).isDisplayed();
		} catch (Exception e) {
			BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Solicitudes de Reembolso')]")).click();
			BaseFlow.driver.findElement(By.xpath("//a[contains(@id,'ext-gen85')]")).click();
			BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Consulta - Rut Beneficiario')]")).isDisplayed();
			//assertTrue(true);
		}
	}

	@When("^Ingreso rut de beneficiario \"([^\"]*)\"$")
	public void ingreso_rut_de_beneficiario(String rutBeneficiario) throws Throwable {
		BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'rutBeneficiario')]")).sendKeys(rutBeneficiario);
		BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'rutBeneficiario')]")).sendKeys(Keys.TAB);
	}
	
	@Then("^Valido si posee Bac activo \"([^\"]*)\"$")
	public void valido_si_posee_Bac_activo(String estadoBac) throws Throwable {
		//MEJORA: REALIZAR CONSULTA DIRECTA DE BAC ACTIVO A BASE DE DATOS
		switch (estadoBac) {
		case "Activo":
			assertTrue("ESTADO BAC ES: ACTIVO", true);
			break;
		case "Inactivo":
			test.setTestCaseId("VTDG-10");
			try {
				BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Beneficiario no tiene caso Activo')]")).isDisplayed();
				test.setMensaje("PRUEBA CORRECTA: Se indica mensaje de Error es caso de que rut ingresado no posea BAC ACTIVO asociado.");
			} catch (Exception e) {
				test.setMensaje("PRUEBA FALLIDA; No existe mensaje indicando que beneficiario NO POSEE BAC ACTIVO");
				assertTrue("NO EXISTE ALERTA PARA RUT INGRESADO SIN BAC",false);
			}
			break;	
		default:
			break;
		}
	}

	@Then("^Valido que se carge nombre y correo de beneficiario$")
	public void valido_que_se_carge_nombre_y_correo_de_beneficiario() throws Throwable {
		//MEJORA: REALIZAR CONSULTA DIRECTA DE BAC ACTIVO A BASE DE DATOS
		test.setTestCaseId("VTDG-11");
		JavascriptExecutor js = (JavascriptExecutor) BaseFlow.driver;
		String nombreAfiliado = (String) js.executeScript("return document.getElementById('nombreBeneficiario').value");
		String correoAfiliado = (String) js.executeScript("return document.getElementById('bac_email').value");
		if (nombreAfiliado.contentEquals("") || correoAfiliado.contentEquals("")) {
			test.setMensaje("PRUEBA FALLIDA: No se cargaron los datos del beneficiario");
			assertTrue(false);
		} else {
			test.setMensaje("PRUEBA CORRECTA: Se cargó nombre y correo de beneficiario con BAC ACTIVO)");
			assertTrue(true);
		}
	}

	@Then("^Valido que nombre y correo no sean editables$")
	public void valido_que_nombre_y_correo_no_sean_editables() throws Throwable {
		test.setTestCaseId("VTDG-12");
		try {			
			BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'desdocu')]")).sendKeys("SeguirEditandoOK");
			test.setMensaje("PRUEBA FALLIDA: es posible editar los datos del beneficiario");
		} catch (Exception e2) {
			test.setMensaje("PRUEBA CORRECTA: Se valida que no se pueden editar los datos del beneficiario");
		}	
	}

	@Then("^Valido que se carguen las solicitudes anteriores del beneficiario$")
	public void valido_que_se_carguen_las_solicitudes_anteriores_del_beneficiario() throws Throwable {
		//MEJORA: REALIZAR CONSULTA DIRECTA DE BAC ACTIVO A BASE DE DATOS
		int solicitudesAnteriores = BaseFlow.driver.findElements(By.xpath("//div[contains(@class,'x-grid3-body')]//tr")).size();
		test.setTestCaseId("VTDG-13");
		if (solicitudesAnteriores > 0) {
			test.setMensaje("PRUEBA CORRECTA: Se cargan solicitudes anteriores del beneficiario");
		} else {
			test.setMensaje("PRUEBA FALLIDA: No se cargaron solicitudes anteriores");
			assertTrue("PRUEBA FALLIDA: No se cargaron solicitudes anteriores", false);
		}
		
	}

	@Then("^valido estructura de columnas de tabla de solicitudes anteriores$")
	public void valido_estructura_de_columnas_de_tabla_de_solicitudes_anteriores() throws Throwable {
		test.setTestCaseId("VTDG-14");
		try {
			BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-hd-bac_idsol')]")).isDisplayed();
			BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-hd-bac_fecsol')]")).isDisplayed();
			BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-hd-bac_benrut')]")).isDisplayed();
			BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-hd-bac_glsestado')]")).isDisplayed();
			BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-hd-bac_glssuc')]")).isDisplayed();
			BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-hd-usu_nombre')]")).isDisplayed();
		} catch (Exception e) {
			test.setMensaje("PRUEBA FALLIDA: No se encontraron todas las columnas de informacion solicitadas");
			assertTrue("PRUEBA FALLIDA: No se encuentra una de las columnas acordadas", false);
		}
	}

	@Given("^Me encuentro en solicitud anterior de beneficiario$")
	public void me_encuentro_en_solicitud_anterior_de_beneficiario() throws Throwable {
		Actions action = new Actions(BaseFlow.driver);
		WebElement link = BaseFlow.driver.findElements(By.xpath("//div[contains(@class,'x-grid3-col-ver')]")).get(0);
		action.doubleClick(link).perform();
	}

	@Then("^Valido si se puede seguir editando$")
	public void valido_si_se_puede_seguir_editando() throws Throwable {
		JavascriptExecutor js = (JavascriptExecutor) BaseFlow.driver;
		String estadoSolicitud = (String) js.executeScript("return document.getElementById('estado').value");
		test.setTestCaseId("VTDG-15");
		switch (estadoSolicitud) {
		case "Enviada a CÍA":
			try {
		    	BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).sendKeys("SeguirEditandoOK");
		    	test.setMensaje("PRUEBA FALLIDA: es posible seguir editando solicitud");
		    	assertTrue(false);
			} catch (Exception e) {
				test.setMensaje("PRUEBA CORRECTA: No se puede Seguir editando solicitud");
			}
			break;
		default:
			try {
		    	BaseFlow.driver.findElement(By.xpath("//input[contains(@id,'bac_des')]")).sendKeys("SeguirEditandoOK");
		    	test.setMensaje("PRUEBA CORRECTA: es posible seguir editando solicitud");
		    	//span[contains(text(),'Ingreso de Gastos (Paso 2)')]
			} catch (Exception e) {
				test.setMensaje("PRUEBA FALLIDA: No se puede Seguir editando solicitud");
				assertTrue("PRUEBA FALLIDA: No se encuentra una de las columnas acordadas", false);
			}
			break;
		}
		
	}
}
