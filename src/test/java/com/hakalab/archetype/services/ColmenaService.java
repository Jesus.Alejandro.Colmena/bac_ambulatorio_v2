package com.hakalab.archetype.services;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.hakalab.archetype.flow.BaseFlow;
import com.hakalab.archetype.util.GenericMethods;

public class ColmenaService {

	public static void ingresaDatosPersonales(String rut, String nombre, String apellido, String email, String celular) throws Exception {
		Thread.sleep(2000);
		GenericMethods.implicityWait(20, By.xpath("//iframe[@class='iframeCode']"));
		BaseFlow.driver.switchTo().frame(BaseFlow.driver.findElement(By.xpath("//iframe[@class='iframeCode']")));
		WebElement txtRut = BaseFlow.driver.findElement(By.xpath("//input[@id='formRut']"));
		txtRut.sendKeys(rut);
		WebElement txtNombre = BaseFlow.driver.findElement(By.xpath("//input[@id='formNombre']"));
		txtNombre.sendKeys(nombre);
		WebElement txtApellido = BaseFlow.driver.findElement(By.xpath("//input[@id='formApellido']"));
		txtApellido.sendKeys(apellido);
		WebElement txtCorreo = BaseFlow.driver.findElement(By.xpath("//input[@id='formCorreo']"));
		txtCorreo.sendKeys(email);
		WebElement txtCelular = BaseFlow.driver.findElement(By.xpath("//input[@id='formCelular']"));
		txtCelular.sendKeys(celular);
	}
}
