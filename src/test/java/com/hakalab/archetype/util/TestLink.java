package com.hakalab.archetype.util;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import junit.framework.Assert;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.cucumber.listener.Reporter;
import com.google.common.io.Files;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.model.Attachment;
import br.eti.kinoshita.testlinkjavaapi.model.Build;
import br.eti.kinoshita.testlinkjavaapi.model.Execution;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import br.eti.kinoshita.testlinkjavaapi.model.TestPlan;
import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;
import cucumber.api.Scenario;
import com.hakalab.archetype.flow.BaseFlow;

public final class TestLink {
	public static String API_KEY = "e5dc6b063d10dbe6a0d1a60b34770d9e"; // Your API
	//http: //localhost/testlink/site/index.php   http://4.0.211.2/testlink/lib/api/xmlrpc/v1/xmlrpc.php
	public static String SERVER_URL = "http://4.0.211.11/lib/api/xmlrpc/v1/xmlrpc.php"; // your testlink server url
	public static String PROJECT_NAME = "Venta Digital";
	public static String TEST_PLAN_NAME = "PruebasTestLink";
	public static String BUILD_NAME = "Ciclo2";
	//public static String PLATAFORM = "";

	

	public static void updateTestCase(String idTestCase,String notaTestCase,Scenario scenario) throws Exception {
		 
	    TestLinkAPI api = new TestLinkAPI(new URL(SERVER_URL), API_KEY);
	    TestCase testCase = api.getTestCaseByExternalId(idTestCase, null);
	 
	    System.out.println("Test Case: "
	        + testCase.getName()
	        + " Internal ID: "
	        + testCase.getInternalId()
	        + " External ID: "
	        + testCase.getId());
	 
	    TestPlan testPlan = api.getTestPlanByName(TEST_PLAN_NAME, PROJECT_NAME);
	 
	    System.out.println("Test Plan: " + testPlan.getName() + " ID: " + testPlan.getId());
	 
	    Build[] builds = api.getBuildsForTestPlan(testPlan.getId());
	 
	    Build build = null;
	 
	    for (Build buildInArray : builds) {
	      if (buildInArray.getName().equals(BUILD_NAME)) {
	        System.out.println("Build found! " + buildInArray.getName() + " " + buildInArray.getId());
	        build = buildInArray;
	        break;
	      }
	    }
	 
	    System.out.println("Builds: " + builds.length + " " + builds[0].getId() + " " + builds[0].getName());
	System.out.println(scenario.getStatus()); 
		
	if (scenario.getStatus().equals("passed")) {
		api.reportTCResult(testCase.getId(), null, testPlan.getId(), ExecutionStatus.PASSED, build.getId(), null,
	    		notaTestCase, null, null, null, null, null, null);
	}else {
		api.reportTCResult(testCase.getId(), null, testPlan.getId(), ExecutionStatus.FAILED, build.getId(), null,
	    		notaTestCase, null, null, null, null, null, null);
	}
	   
	    
	   
	    
	    Thread.sleep(2000);
	   screenShotForScenario(scenario);
	    String folderPath = System.getProperty("user.dir")+"/Reporte/imagenesValidaciones/"+"/"; 
	    
		File attachmentFile = new File(folderPath + scenario.getName() + ".png");

		String fileContent = null;

		try {
		    byte[] byteArray = FileUtils.readFileToByteArray(attachmentFile);
		    fileContent = new String(Base64.encodeBase64(byteArray));
		} catch (IOException e) {
		    e.printStackTrace(System.err);
		    System.exit(-1);
		}
		
		Execution nn=new Execution();
		nn=api.getLastExecutionResult(testPlan.getId(), testCase.getId(), testCase.getId());
	System.out.println("id execution : "+nn.getId());

		Attachment attachment = api.uploadExecutionAttachment(nn.getId(), // executionId
			scenario.getName(), // title
			"El estado del scenario es : "+scenario.getStatus(), // description
			scenario.getName() + System.currentTimeMillis()
				+ ".jpg", // fileName
			"image/jpeg", // fileType
			fileContent); // content

		System.out.println("Attachment uploaded");
		
		Assert.assertNotNull(attachment);
	   
	  }
	
	public static void screenShotForScenario(Scenario scenario) throws Exception {
		
		
		File sourcePath = ((TakesScreenshot) BaseFlow.driver).getScreenshotAs(OutputType.FILE);
		String folderPath = System.getProperty("user.dir")+"/Reporte/imagenesValidaciones/"+"/";
		File folder = new File(folderPath);
		DeleteFileIfExist(folder);
		folder.mkdir();
		File destinationPath = new File(folderPath + scenario.getName() + ".png");
		Files.copy(sourcePath, destinationPath);
		Reporter.addScreenCaptureFromPath(destinationPath.toString(),scenario.getName());	
}
	
	private static void DeleteFileIfExist(File file) {
	    if (!file.exists()) {
	    	file.delete();
	    } 
	}
	
}
