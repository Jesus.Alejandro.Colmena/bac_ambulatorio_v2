package com.hakalab.archetype.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sybase.jdbc3.jdbc.SybDriver;


public class DbConnect {

	private static Connection con = null;
	 
	static final String DRIVER_SQL = "oracle.jdbc.driver.OracleDriver";
	static final String DATABASE_URLJournal = "jdbc:oracle:thin:@200.14.169.234:1521/ORION";
	static final String USERJorunal = "JOURNAL_CN";
	static final String PASSJorunal = "JOURNAL_CN_ORION2K16";

	static final String DATABASE_URL_MDM = "jdbc:oracle:thin:@200.14.169.238:1521/ODIN";
	static final String USERMDM = "BCH_MDMXT";
	static final String PASSMDM = "BCH_MDMXT_ODIN2K17";

	/**
	 * Function: getConnection Description: Retorna el controlador de Base de datos
	 **/

	public static Connection getConnectionJournal() {
		try {

			Class.forName(DRIVER_SQL);
			con = DriverManager.getConnection(DATABASE_URLJournal, USERJorunal, PASSJorunal);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}

	public static Connection getConnectionMDM() {
		try {

			Class.forName(DRIVER_SQL);
			con = DriverManager.getConnection(DATABASE_URL_MDM, USERMDM, PASSMDM);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
	
	
	public static Connection getConnectionSybase() {
		try {
		//com.sybase.jdbc2.jdbc.SybDriver 
			//com.sybase.jdbc3.jdbc.SybDriver
			Class.forName("com.sybase.jdbc3.jdbc.SybDriver");
		       String url = "jdbc:sybase:Tds:4.0.11.2:20000/isapre";
		       con = DriverManager.getConnection(url,"cerjes","188826");
		       // = con.createStatement();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
	
	 public static List<?> getFolioBono() { 
	        List<String> lista = new ArrayList<>(); 
	        try { 
	            ResultSet rs = null; 
	            con = DbConnect.getConnectionSybase(); 
	            String query = "SELECT TOP 20 fld_bonfolio FROM BON WHERE fld_bonestado IN (3, 4) ORDER BY fld_bonfolio DESC"; 
	            PreparedStatement qQuery = con.prepareStatement(query); 
	            rs = qQuery.executeQuery(); 
	            if (rs.next()) { 
	                lista.add(rs.getString("fld_bonfolio")); 
	            } 
	            con.close(); 
	        } catch (Exception e) { 
	            e.printStackTrace(); 
	        } 
	        return lista; 
	    } 
	
}
