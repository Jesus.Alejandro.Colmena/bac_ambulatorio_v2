package com.hakalab.archetype.util;

import java.io.File;
import java.io.IOException;
import java.text.Normalizer;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;
import com.google.common.io.Files;
import com.hakalab.archetype.flow.BaseFlow;

import cucumber.api.Scenario;

public final class GenericMethods {
	private static final Log log = LogFactory.getLog(GenericMethods.class);

    public static void waitTime(WebElement object) throws InterruptedException {
        int i = 0;
        do {
            Thread.sleep(i + 1);
        } while (object.isDisplayed() == false);
    }
    
    public static WebElement implicityWait(Integer timeoutInSeconds,By element)throws Exception{
    	WebElement elemento = null;
    	try {
    		WebDriverWait wait = new WebDriverWait(BaseFlow.driver, timeoutInSeconds); 
        	elemento = wait.until(ExpectedConditions.visibilityOfElementLocated(element));
		}catch (Exception e) {
			log.info(e);
		}
    	return elemento;
    }
    
    public static Boolean isVisibleElement(Integer timeoutInSeconds,WebElement element)throws Exception{
    	Boolean isVisible = false;
    	try {
    		WebDriverWait wait = new WebDriverWait(BaseFlow.driver, timeoutInSeconds); 
        	isVisible = wait.until(ExpectedConditions.visibilityOf(element))!=null?true:false;
		}catch (Exception e) {}
    	return isVisible;
    }
    
    public static Boolean isVisibleElement(Integer timeoutInSeconds,By by)throws Exception{
    	Boolean isVisible = false;
    	try {
    		WebElement element = BaseFlow.driver.findElement(by);
    		WebDriverWait wait = new WebDriverWait(BaseFlow.driver, timeoutInSeconds); 
        	isVisible = wait.until(ExpectedConditions.visibilityOf(element))!=null?true:false;
		}catch (Exception e) {}
    	return isVisible;
    }
    
    
    public static WebElement waitForClickeable(Integer timeoutInSeconds,WebElement element)throws Exception{
    	WebElement elemento = null;
    	try {
    		WebDriverWait wait = new WebDriverWait(BaseFlow.driver, timeoutInSeconds); 
        	elemento = wait.until(ExpectedConditions.elementToBeClickable(element));
		}catch (Exception e) {
			log.info(e);
			throw e;
		}
    	return elemento;
    }
    
    public static WebElement waitForClickeable(Integer timeoutInSeconds,By element)throws Exception{
    	WebElement elemento = null;
    	try {
    		WebDriverWait wait = new WebDriverWait(BaseFlow.driver, timeoutInSeconds); 
        	elemento = wait.until(ExpectedConditions.elementToBeClickable(element));
		}catch (Exception e) {
			log.info(e);
			throw e;
		}
    	return elemento;
    }
    
    
    public static Boolean existElement(By element) throws InterruptedException { 
    	boolean isPresent = false; 
    	for (int i = 0; i < 1; i++) {
    	 try { 
    		 	if (BaseFlow.driver.findElement(element) != null) 
    		 	{ 
    		 		isPresent = true; 
    		 		break; 
    	 		} 
     		} catch (Exception e) { 
     			Thread.sleep(500); 
    		} 
    	} 
    	return isPresent;
    }
    
    public static WebElement findElementReturnWebElement(By element) throws InterruptedException { 
    	WebElement webElement = null;
    	for (int i = 0; i < 4; i++) {
    	 try { 
    		 	if (BaseFlow.driver.findElement(element) != null) 
    		 	{ 
    		 		webElement =  BaseFlow.driver.findElement(element);
    	 		} 
     		} catch (Exception e) { 
     			System.out.println(e.getLocalizedMessage()); 
     			Thread.sleep(1000); 
    		} 
    	}
		return webElement; 
    }
    
    
        
	public static void clickElement(WebElement element) {
		try {
			((JavascriptExecutor) BaseFlow.driver).executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * Metodo que se encarga de esperar X cantidad de segundos durante la ejecucion de la prueba utilizando el driver
	 * 
	 * @param driver
	 * @param segundos
	 */
	public static void esperarSegundos(int segundos) {
		synchronized (BaseFlow.driver) {
			try {
				BaseFlow.driver.wait(segundos * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Devolver path con rutas ya configuradas para Chrome
	 * 
	 */
	public static String getProperty() {
		String dirDriver = System.getProperty("user.dir");
		String path = dirDriver + "\\driver\\Chrome\\Windows\\chromedriver.exe";
		return path;
	}

	/**
	 * Devolver path con rutas ya configuradas para IExplorer
	 * 
	 */
	public static String getPropertyIE() {
		String dirDriver = System.getProperty("user.dir");
		String path = dirDriver + "\\driver\\IExplorer\\Windows\\64\\IEDriverServer.exe";
		return path;
	}

	/**
	 * Fecha actual
	 */
	public static String now() {
		Calendar ahora = Calendar.getInstance();
		Integer n = new Integer((ahora.get(Calendar.YEAR) * 10000) + ((ahora.get(Calendar.MONTH) + 1) * 100) + (ahora.get(Calendar.DAY_OF_MONTH)));
		return n.toString();
	}

	/**
	 * Cambiar el foco de ventana web
	 * 
	 * @param driver
	 * @param cambiarSoloUna
	 *            En caso de haber mas de dos ventana y solo querer cambiar a la siguiente, true
	 */
	public static void cambiarVentanaWeb(boolean cambiarSoloUna) {
		String MainWindow = BaseFlow.driver.getWindowHandle();
		Set<String> s1 = BaseFlow.driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();

		while (i1.hasNext()) {
			String ChildWindow = i1.next();
			if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
				BaseFlow.driver.switchTo().window(ChildWindow);
				if (cambiarSoloUna)
					break;
			}
		}
	}

	/**
	 * Cerrar ventana web, solo cierra ventana sin detener driver
	 * 
	 * @param driver
	 *            WebDriver
	 */
	public static void cerrarVentanaWeb() {
		String MainWindow = BaseFlow.driver.getWindowHandle();
		Set<String> s1 = BaseFlow.driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();

		while (i1.hasNext()) {
			String ChildWindow = i1.next();
			if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
				BaseFlow.driver.close();
				BaseFlow.driver.switchTo().window(ChildWindow);
			}
		}
	}

	
	
	/**
	 * Sroll hasta final de pagina
	 * 
	 * @param driver
	 *            WebDriver
	 * 
	 * @return void
	 */
	public static void finalVentana() {
		((JavascriptExecutor) BaseFlow.driver).executeScript("window.scrollBy(0,500)");
	}

	/**
	 * scrollElement, realiza scroll de pagina en donde se encuentra objeto
	 * 
	 * @param driver
	 *            WebDriver
	 * @param element
	 *            WebElement donde se realizara el Scroll
	 * 
	 * @return void
	 */
	public static void scrollElement( WebElement element) {
		((JavascriptExecutor) BaseFlow.driver).executeScript("arguments[0].scrollIntoView();", element);
	}

	public static void scrollDownPage() {
		((JavascriptExecutor) BaseFlow.driver).executeScript("window.scrollTo(0, document.body.scrollHeight);");
	}
	
	/**
	 * scrollClickElement, realiza scroll y ademas click al elemento de la pagina
	 * 
	 * @param element
	 *            WebElement donde realizara Scroll y hara click
	 * 
	 * @return void
	 */
	public static void scrollClickElement(WebElement element) {
		try {
			((JavascriptExecutor) BaseFlow.driver).executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * obtenerTextoNovisible, retorna el texto de un elemento que no esta visible como por ejemplo en un div donde hay lista de objetos y estos no se visualizan todos
	 * 
	 * @param driver
	 *            WebDriver
	 * @param element
	 *            WebElement que se obtendra el texto
	 * 
	 * @return String
	 */
	public static String obtenerTextoNoVisible(WebElement element) {
		String texto = "";
		try {
			texto = element.getText().toString().trim();
			texto = (String) ((JavascriptExecutor) BaseFlow.driver).executeScript("return arguments[0].innerText", element);
		} catch (Exception e) {
			texto = "";
		}
		return texto;
	}
	
	public static String obtenerValorElemento(WebElement element) {
		String texto = "";
		try {
			texto = (String) ((JavascriptExecutor) BaseFlow.driver).executeScript("return arguments[0].value", element);
		} catch (Exception e) {
			texto = "";
		}
		return texto;
	}
	
	

	/**
	 * ingresarCaracteresEspecialesID, en algunos navegadores no ingresa caracteres especiales como @ de forma correcta, este metodo mitiga ese conflicto
	 * 
	 * @param driver
	 *            WebDriver
	 * @param element
	 *            WebElement al cual se ingresara frase o caracter especial
	 * @param id
	 *            Identificador ID del elemento al que se ingresara caracter
	 * @param caracterEspecial
	 *            Frase o caracter especial a ingresar
	 * 
	 * @return void
	 */
	public static void ingresarCaracteresEspecialesID( WebElement element, String id, String caracterEspecial) {
		try {
			((JavascriptExecutor) BaseFlow.driver).executeScript(String.format("document.getElementById(\"" + id + "\").value=\"" + caracterEspecial + "\";", element));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean unloadWebElementByClass(WebElement webElement, int segundos) {
		Boolean elementExist = isElementPresent(webElement);
		int conTiempo = 0;
		String claseTag = "";
		if (elementExist)
			claseTag = webElement.getAttribute("class");
		while (elementExist == true && conTiempo <= segundos) {
			elementExist = (Boolean) ((JavascriptExecutor) BaseFlow.driver).executeScript("var elemento = document.getElementsByClassName('" + claseTag + "');return (elemento.length == 0)?false:true;");
			esperarSegundos(2);
			conTiempo = conTiempo + 2;
		}
		return elementExist;
	}

	/**
	 * Comprobar si despliega alert y cerrar este mismo
	 * 
	 * @param driver
	 * @param aceptar
	 *            Para casos de confirm se puede aceptar o continuar con operacion
	 */
	public static void checkAlert( boolean aceptar) {
		try {
			WebDriverWait wait = new WebDriverWait(BaseFlow.driver, 5);
			wait.until(ExpectedConditions.alertIsPresent());
			if (BaseFlow.driver.switchTo().alert() != null) {
				Alert alert = BaseFlow.driver.switchTo().alert();
				if (aceptar) 
					alert.accept();
				else 
					alert.dismiss();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Comprobar si despliega alert, obtiene mensaje de este y luego lo cierra
	 * 
	 * @param driver
	 */
	public static String textoAlert() {
		String texto = "";
		try {
			WebDriverWait wait = new WebDriverWait(BaseFlow.driver, 10);
			wait.until(ExpectedConditions.alertIsPresent());
			if (BaseFlow.driver.switchTo().alert() != null) {
				Alert alert = BaseFlow.driver.switchTo().alert();
				texto = alert.getText().toString().trim();
				alert.accept();
			}
		} catch (Exception e) {
			texto = "";
			e.printStackTrace();
		}
		return texto;
	}

	/**
	 * Cerrar ventana WebDriver
	 * 
	 * @param driver
	 */
	public static void cerrarVentana() {
		BaseFlow.driver.close();
	}

	/**
	 * Cerrar proceso WebDriver
	 * 
	 * @param driver
	 */
	public static void cerrarDriver() {
		BaseFlow.driver.quit();
	}

	/**
	 * Volver atras
	 * 
	 * @param driver
	 */
	public static void volver() {
		BaseFlow.driver.navigate().back();
	}

	/**
	 * Comprobar si el valor es numerico
	 * 
	 * @param valor
	 *            Parametro a comprobar si es numerico
	 */
	public static boolean isNumeric(String valor) {
		try {
			Double.parseDouble(valor);
			return true;
		} catch (NumberFormatException ne) {
			return false;
		}
	}

	/**
	 * <b>Nombre:</b> esperaElementoSegundos</br>
	 * </br>
	 * <b>Description:</b> Genera una pausa explicita hasta que el elemento dado es encontrado.
	 * 
	 * @param WebDriver
	 *            Controlador WebDrive.
	 * @param WebElement
	 *            Elemento a esperar.
	 * @param segundos
	 *            (int) Valor de tiempo en segundos a esperar.
	 * @return {@link Boolean} Retorna un valor <b>verdadero</b> si el elemento es encontrado dentro del tiempo estipulado, de lo contrario retorna un valor <b>falso</b>.
	 **/
	public static boolean esperaElementoSegundos(WebElement webElement, int segundos) {
		WebDriverWait wait = new WebDriverWait(BaseFlow.driver, segundos);
		try {
			wait.until(ExpectedConditions.visibilityOf(webElement));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}

	/**
	 * <b>Nombre:</b> esperaElementoSegundos</br>
	 * </br>
	 * <b>Description:</b> Genera una pausa explicita hasta que lista de elementos dados sean encontrados.
	 * 
	 * @param driver
	 *            Controlador WebDrive.
	 * @param listElement
	 *            Lista de elementos a esperar.
	 * @param segundos
	 *            (int) Valor de tiempo en segundos a esperar.
	 * @return {@link Boolean} Retorna un valor <b>verdadero</b> si el elemento es encontrado dentro del tiempo estipulado, de lo contrario retorna un valor <b>falso</b>.
	 **/
	public static boolean esperaElementosSegundos( List<WebElement> listElement, int segundos) {
		WebDriverWait wait = new WebDriverWait(BaseFlow.driver, segundos);
		try {
			wait.until(ExpectedConditions.visibilityOfAllElements(listElement));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}

	/**
	 * <b>Nombre:</b> notEsperaElementoSegundos</br>
	 * </br>
	 * <b>Description:</b> Genera una pausa explicita hasta que el elemento dado desaparece.
	 * 
	 * @param {@link
	 * 			WebDriver} Controlador WebDrive.
	 * @param {@link
	 * 			WebElement} Elemento a esperar.
	 * @param segundos
	 *            (int) Valor de tiempo en segundos a esperar.
	 * @return {@link Boolean} Retorna un valor <b>verdadero</b> si el elemento es encontrado dentro del tiempo estipulado, de lo contrario retorna un valor <b>falso</b>.
	 **/
	public static boolean notEsperaElementoSegundos(WebElement webElement, int segundos) {
		WebDriverWait wait = new WebDriverWait(BaseFlow.driver, segundos);
		if (isElementPresent(webElement)) {
			try {
				wait.until(ExpectedConditions.not(ExpectedConditions.visibilityOf(webElement)));
				return true;
			} catch (TimeoutException e) {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * <b>Nombre:</b> esperaElementoException</br>
	 * </br>
	 * <b>Description:</b> Genera una pausa explicita hasta que el elemento dado es encontrado.
	 * 
	 * @param {@link
	 * 			WebDriver} Controlador WebDrive.
	 * @param {@link
	 * 			WebElement} Elemento a esperar.
	 * @param segundos
	 *            (int) Valor de tiempo en segundos a esperar.
	 * @return {@link Void} Retorna un <b>TimeoutException</b> si el elemento NO es encontrado dentro del tiempo estipulado, de lo contrario solo continua con el flujo normal.
	 **/
	public static void esperaElementoException( WebElement webElement, int segundos) {
		WebDriverWait wait = new WebDriverWait(BaseFlow.driver, segundos);
		wait.until(ExpectedConditions.visibilityOf(webElement));
	}

	/**
	 * <b>Nombre:</b> isElementPresent</br>
	 * </br>
	 * <b>Description:</b> Verifica la existencia de un elemento
	 * 
	 * @param {@link
	 * 			WebElement} Objeto de tipo WebElement a buscar
	 * @return {@link Boolean} Retorna <b>True</b> si el elemento es encontrado, de lo contrario retorna <b>False</b>
	 **/
	public static boolean isElementPresent(WebElement webElement) {
		boolean resp = false;
		try {
			resp = webElement.isDisplayed();
		} catch (NoSuchElementException e) {
			resp = false;
		}
		return resp;
	}
	
	/**
	 * <b>Nombre:</b> isClickable</br>
	 * </br>
	 * <b>Description:</b> Comprueba si elemento es posible hacerle click
	 * 
	 * @param element
	 *            WebElement a verificar
	 * 
	 * @return boolean
	 **/
	public static boolean isClickable(Integer timeout,WebElement element) {
		try {
			WebDriverWait wait = new WebDriverWait(BaseFlow.driver, timeout);
			wait.until(ExpectedConditions.elementToBeClickable(element));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static String dateAdd(int intervalo, int numero, String fecha) {
		int ano = ((new Integer(fecha)).intValue() / 10000);
		int mes = (((new Integer(fecha)).intValue() - (ano * 10000)) / 100);
		int dia = (((new Integer(fecha)).intValue() - (ano * 10000) - (mes * 100)));

		Calendar f = Calendar.getInstance();
		f.set(ano, mes - 1, dia);

		f.add(intervalo, numero);

		String anoStr = String.valueOf(f.get(Calendar.YEAR));
		String mesStr = String.valueOf(f.get(Calendar.MONTH) + 1);
		String diaStr = String.valueOf(f.get(Calendar.DAY_OF_MONTH));

		anoStr = "0000" + anoStr.trim();
		mesStr = "00" + mesStr.trim();
		diaStr = "00" + diaStr.trim();

		anoStr = anoStr.substring(anoStr.length() - 4);
		mesStr = mesStr.substring(mesStr.length() - 2);
		diaStr = diaStr.substring(diaStr.length() - 2);

		String fechanueva = anoStr + mesStr + diaStr;

		return fechanueva;
	}

	public static long dateDiff(String fechaInicio, String fechaFin) {
		return dateDiff(fechaInicio, fechaFin, 0);
	}

	public static long dateDiff(String fechaInicio, String fechaFin, int intervalo) {
		Calendar inicio = Calendar.getInstance();
		Calendar fin = Calendar.getInstance();
		inicio.set(Integer.parseInt(fechaInicio.substring(0, 4)), Integer.parseInt(fechaInicio.substring(4, 6)) - 1, Integer.parseInt(fechaInicio.substring(6, 8)));
		fin.set(Integer.parseInt(fechaFin.substring(0, 4)), Integer.parseInt(fechaFin.substring(4, 6)) - 1, Integer.parseInt(fechaFin.substring(6, 8)));
		inicio.add(Calendar.DAY_OF_MONTH, +intervalo);
		return fin.getTimeInMillis() - inicio.getTimeInMillis();
	}

	public static long diferenciaDias(String fechaInicio, String fechaFin) {
		long dias = 0;
		dias = dateDiff(fechaInicio, fechaFin) / (3600 * 24 * 1000);
		return dias;
	}

	public static String formateaFecha(String fecha) {
		try {
			String fechaFormato = fecha.substring(6, 8);
			fechaFormato += "/" + fecha.substring(4, 6);
			fechaFormato += "/" + fecha.substring(0, 4);
			return fechaFormato;
		} catch (Exception e) {
			return fecha;
		}
	}

	/**
	 * Crear digito verificador mediante numeros x entregados
	 * 
	 * @param r
	 *            digitos para generar rut
	 * @return String
	 */
	public static String generaDvRut(String r) {
		String dv = "";
		int rut, digito, suma, resto, resultado, factor;
		rut = Integer.parseInt(r);
		for (factor = 2, suma = 0; rut > 0; factor++) {
			digito = rut % 10;
			rut /= 10;
			suma += digito * factor;
			if (factor >= 7)
				factor = 1;
		}
		resto = suma % 11;
		resultado = 11 - resto;
		if (resultado < 10)
			dv = String.valueOf(resultado);
		else if (resultado == 10)
			dv = "K";
		else
			dv = "0";

		return dv;
	}
	
	public static Boolean existWebElement(WebElement elementoLista, By element) throws InterruptedException { 
        boolean isPresent = false; 
         try {  
    	 		 if (elementoLista.findElement(element) != null) { 
    	 			 isPresent = true; 
    	 		 }
             } catch (Exception e) { 
                 Thread.sleep(1000); 
            } 
         
        return isPresent;
    }
	
	public static boolean getStatusHref(WebElement element) {
        String href = element.getAttribute("href");
        boolean status;
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(href);
        HttpResponse response = null;
        try {
            response = client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int code = response.getStatusLine().getStatusCode();
        if (code == 200 || code == 401) {
            status = true;
        } else {
            status = false;
        }
        return status;
    }
	
	/**
	 * 
	 * @param scenario
	 * @throws Exception
	 */
	public static void screenShotForAllScenario(Scenario scenario) throws Exception {
	    try {
			File sourcePath = ((TakesScreenshot) BaseFlow.driver).getScreenshotAs(OutputType.FILE);
			String folderPath = System.getProperty("user.dir") + "/Reporte/img/";
			File folder = new File(folderPath);
			createdFolderIfExist(folder);
			String nameImg = scenario.getName()+"-"+getDate()+ ".png";
			File destinationPath = new File(folderPath+nameImg);
			Files.copy(sourcePath, destinationPath);
			Reporter.addScreenCaptureFromPath("../Reporte/img/"+nameImg, scenario.getName());
			System.out.println("name screenshot: "+nameImg);
	    } catch (Exception e) {
	        System.out.println("Error al tomar screenshot"+e);
	    }	
	}
	
	private static void createdFolderIfExist(File folder) {
		if (!folder.exists()) {
			folder.mkdir();
		}
	}
	
	private static void DeleteFileIfExist(File file) {
		if (file.exists()) {
			file.delete();
		}
	}

	
	public static void screenShotForAllScenario(String nameImg) throws Exception {
	    try {
	    	File sourcePath = ((TakesScreenshot) BaseFlow.driver).getScreenshotAs(OutputType.FILE);
			String folderPath = System.getProperty("user.dir") + "/Reporte/img/" + "/";
			File folder = new File(folderPath);
			GenericMethods.DeleteFileIfExist(folder);
			folder.mkdir();
			nameImg = nameImg+"-"+getDate()+ ".png";
			File destinationPath = new File(folderPath+nameImg);
			Files.copy(sourcePath, destinationPath);
			Reporter.addScreenCaptureFromPath("../Reporte/img/"+nameImg);
	    } catch (Exception e) {
	        System.out.println("Error al tomar screenshot"+e);
	    }	
	}
	
	public static String getDate() {
		Calendar fecha = new GregorianCalendar();
		Integer annio = fecha.get(Calendar.YEAR);
		Integer mes = fecha.get(Calendar.MONTH) + 1;
		Integer dia = fecha.get(Calendar.DAY_OF_MONTH);
		Integer hora = fecha.get(Calendar.HOUR_OF_DAY);
		Integer minuto = fecha.get(Calendar.MINUTE);
		Integer segundo = fecha.get(Calendar.SECOND);
		String anio = String.valueOf(annio) + String.valueOf(mes) + String.valueOf(dia);
		String horaTotal = String.valueOf(hora) + String.valueOf(minuto) + String.valueOf(segundo);
		return anio + "_" + horaTotal;
	}
	public static String cleanString(String texto) {
		texto = Normalizer.normalize(texto, Normalizer.Form.NFD);
		texto = texto.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		return texto;
    }
	
	public static WebElement getParentNode(WebElement element) {
		WebElement obj  = null;
		try {
			obj =  (WebElement) ((JavascriptExecutor) BaseFlow.driver).executeScript("return arguments[0].parentNode;", element);
		} catch (Exception e) {
			obj = null;
		}
		return obj;
	}
	
	public static String generarRut() {
		String rut = String.valueOf(new Random().nextInt(9999999) + 10000000);
		int multi = 2;
		int acum = 0;
		for (int i = rut.length() - 1; i > -1; i--) {
			if (multi > 7) {
				multi = 2;
			}
			acum += multi * Integer.parseInt(rut.substring(i, i + 1));
			multi++;
		}
		int dv = 11 - (acum % 11);
		String dvf = "";
		if (dv > 9) {
			if (dv == 10) {
				dvf = "K";
			}
			if (dv == 11) {
				dvf = "0";
			}
		} else {
			dvf = String.valueOf(dv);
		}
		return rut + "-" + dvf;
	}
	
	/**
	 * textoAlertBuscador Devuelve el texto de las alertas que aparecen en la ejecución actual, cierra el mensaje.
	 * @return
	 */
	public static String textoAlertBuscador() {
		String texto = "";
		try {
			WebDriverWait wait = new WebDriverWait(BaseFlow.driver, 10);
			wait.until(ExpectedConditions.alertIsPresent());
			if (BaseFlow.driver.switchTo().alert() != null) {
				Alert alert = BaseFlow.driver.switchTo().alert();
				texto = alert.getText().toString().trim();
				alert.accept();
			}
		} catch (Exception e) {
			texto = "";
			e.printStackTrace();
		}
		return texto;
	}
	
	/**
	 * mouseOver, realiza mouOvera elemento 
	 * 
	 * @param element
	 *            WebElement donde realizara mouseover
	 * 
	 * @return void
	 */
	public static void mouseOverObject(WebElement element) {
		try {
			String javaScript = "var evObj = document.createEvent('MouseEvents');" +
                    "evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);" +
                    "arguments[0].dispatchEvent(evObj);";
			((JavascriptExecutor) BaseFlow.driver).executeScript(javaScript, element);
			System.out.println("realizo mouseOverss");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void highlightElementBy (By by) throws Exception {
		try {
			WebElement element = BaseFlow.driver.findElement(by);
			GenericMethods.scrollElement(element);
			JavascriptExecutor js=(JavascriptExecutor)BaseFlow.driver;
			js.executeScript("arguments[0].setAttribute('style', 'border: 3px solid red;');", element);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public static void highlightElement (WebElement element) throws Exception {
		try {
			GenericMethods.scrollElement(element);
			JavascriptExecutor js=(JavascriptExecutor)BaseFlow.driver;
			js.executeScript("arguments[0].setAttribute('style', 'border: 3px solid red;');", element);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	//TestLink Capture Fail Test
	public static void screenShotForScenarioFailed(Scenario scenario) throws Exception {
		if (scenario.isFailed()) {

			File sourcePath = ((TakesScreenshot) BaseFlow.driver).getScreenshotAs(OutputType.FILE);
			String folderPath = System.getProperty("user.dir") + "/Reporte/img/" + "/";
			File folder = new File(folderPath);
			GenericMethods.DeleteFileIfExist(folder);
			folder.mkdir();
			File destinationPath = new File(folderPath + scenario.getName() + ".png");
			Files.copy(sourcePath, destinationPath);
			Reporter.addScreenCaptureFromPath(destinationPath.toString(), scenario.getName());
		}
	}

	
}
