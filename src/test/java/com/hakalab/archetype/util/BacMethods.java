package com.hakalab.archetype.util;

import static org.junit.Assert.assertTrue;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import org.openqa.selenium.By;
import com.hakalab.archetype.flow.BaseFlow;


public class BacMethods {
	//Metodo para adjuntar archivos segun tipo (Paso 2: Gastos)
	public static void adjuntarArchivo (String tipoArchivo) throws Exception {
		switch (tipoArchivo) {
		case "gasto":
			//Setear ruta de archivo de gasto OK
			tipoArchivo = "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\GastoOK.pdf";
			break;
		case "gastoSuperior5mb":
			//Setear ruta de archivo de gasto OK
			tipoArchivo = "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\gastoSuperior5mb.pdf";
			break;
		case "word":
			tipoArchivo = "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\DocumentoWord.docx";
			break;
		case "excel":
			tipoArchivo = "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\DocumentoExcel.xlsx";
			break;
		case "xml":
			tipoArchivo = "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\DocumentoXml.xml";
			break;
		case "pdf":
			tipoArchivo = "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\GastoOK.pdf";
			break;
		case "txt":
			tipoArchivo = "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\FormularioBac.txt";
			break;
		case "jpeg":
			tipoArchivo = "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\CoberturaBAC.jpg";
			break;
		default:
			assertTrue("Ruta de archivo no encontrada", false);
			break;
		}
		//STRINGSELECTION PARA COPIAR LA RUTA AL DIALOGO DE WINDOWS
		StringSelection stringSelection = new StringSelection(tipoArchivo);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		//CLASE ROBOT
		Robot robot = new Robot();	
		//SIN UN TIEMPO DE ESPERA COPIARA LA RUTA MUY RAPIDO Y NO ESPERARA EL DESPLIEGE DE LA VENTANA DE WINDOWS
		Thread.sleep(3000); 
		//CLASE ROBOT KEY PRESS PRESIONA LAS TECLAS INDICADAS
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyPress(KeyEvent.VK_ENTER);
		//CLASE ROBOT KEY RELEASE SUELTA LAS TECLAS O QUEDARAN PRESIONADAS 	
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_ENTER);	
		Thread.sleep(2000); 
	}	
	
	
	public static boolean crearSolicitudBac() throws InterruptedException {
		if (GenericMethods.existElement(By.xpath("//button[contains(text(),'Crear Solicitud')]"))) {
			BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Crear Solicitud')]")).click();
			BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Sí')]")).click();
			BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Aceptar')]")).click();
			return true;
		} else {
		 return false;
		}
	}
}
