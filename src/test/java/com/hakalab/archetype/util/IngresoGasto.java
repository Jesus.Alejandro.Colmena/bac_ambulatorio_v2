package com.hakalab.archetype.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.hakalab.archetype.flow.BaseFlow;

public class IngresoGasto {


	public static void meEncuentroEnPaso (int paso) {
		 try {
			    BaseFlow.driver.findElement(By.xpath("//Button[contains(text(),'Crear Solicitud')]")).click();
				BaseFlow.driver.findElement(By.xpath("//Button[contains(text(),'Sí')]")).click();
				BaseFlow.driver.findElement(By.xpath("//Button[contains(text(),'Aceptar')]")).click();	
		  } catch (Exception e) {
			  BaseFlow.driver.findElement(By.xpath("//span[contains(text(),'Ingreso de Gastos (Paso 2)')]")).click();
		  }	
	}
	
	public static boolean adjuntarArchivo (String archivo) throws Exception  {
		try {
			try {
				BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Aceptar')]")).click();
				BaseFlow.driver.findElement(By.xpath("//div[contains(@id,'x-form-el-documentoGeneral1')]")).click();
				BacMethods.adjuntarArchivo(archivo);
				BaseFlow.driver.findElement(By.xpath("//table[contains(@id,'btnAgregaDocumento1')]")).click();
				Thread.sleep(2000);
			} catch (Exception e) {
				BaseFlow.driver.findElement(By.xpath("//div[contains(@id,'x-form-el-documentoGeneral1')]")).click();
				BacMethods.adjuntarArchivo(archivo);
				BaseFlow.driver.findElement(By.xpath("//table[contains(@id,'btnAgregaDocumento1')]")).click();
				Thread.sleep(2000);
			}
			return true;
		} catch (Exception e) {
			System.out.println(e.toString());
			return false;	
		}	
	}
		
	public static boolean limpiarTabla () throws Exception{
		try {
			int cantidadAdjuntos = BaseFlow.driver.findElements(By.xpath("//div[contains(@class,'x-grid3-cell-inner x-grid3-col-eliminar')]")).size();
			while (cantidadAdjuntos > 0) {
				Actions action = new Actions(BaseFlow.driver);
				WebElement link = BaseFlow.driver.findElements(By.xpath("//div[contains(@class,'x-grid3-cell-inner x-grid3-col-eliminar')]")).get(cantidadAdjuntos-1);
				action.doubleClick(link).perform();	
				BaseFlow.driver.findElement(By.xpath("//button[contains(text(),'Sí')]")).click();
				cantidadAdjuntos--;
			}	
			return true;
		} catch (Exception e) {
			System.out.println(e.toString());
			return false;
		}		
	} 
	
	public static boolean ValidarCobertura (String prestacion) {
			switch (prestacion) {
			case "sinCobertura":
				try {
					BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-col-bac_glssincobertura')][contains(text(),'Si')]")).isDisplayed();
					BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-col-bac_glsnoarancelada')][contains(text(),'No')]")).isDisplayed();
					BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-col-bac_glsotracobertura')]")).isDisplayed();
					return true;
				} catch (Exception e) {
					return false;
				}
			case "noArancelada":
				try {
					BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-col-bac_glssincobertura')][contains(text(),'No')]")).isDisplayed();
					BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-col-bac_glsnoarancelada')][contains(text(),'Si')]")).isDisplayed();
					BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-col-bac_glsotracobertura')]")).isDisplayed();
					return true;
				} catch (Exception e) {
					return false;
				}			
			case "otraCobertura":
				try {
					BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-col-bac_glssincobertura')][contains(text(),'No')]")).isDisplayed();
					BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-col-bac_glsnoarancelada')][contains(text(),'No')]")).isDisplayed();
					BaseFlow.driver.findElement(By.xpath("//div[contains(@class,'x-grid3-col-bac_glsotracobertura')][contains(text(),'Si')]")).isDisplayed();
					return true;
				} catch (Exception e) {
					return false;
				}			
			default:
				return false;		 
			} 
	}	
	
	
	
}
