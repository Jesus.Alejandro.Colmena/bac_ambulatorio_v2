package com.hakalab.archetype.util;

public class TestCaseSingleton {
	public static String testCaseId;
	public static String mensaje;
	
public static String getTestCaseId() {
		return testCaseId;
	}

	public static void setTestCaseId(String testCaseId) {
		TestCaseSingleton.testCaseId = testCaseId;
	}

	public static String getMensaje() {
		return mensaje;
	}

	public static void setMensaje(String mensaje) {
		TestCaseSingleton.mensaje = mensaje;
	}

	

static TestCaseSingleton instance = null;
	
	public static void setInstance(TestCaseSingleton instance) {
		TestCaseSingleton.instance = instance;
	}

	public static TestCaseSingleton getInstance() {
		if (instance == null) {
			instance = new TestCaseSingleton();
		}
		return instance;
	}
}
