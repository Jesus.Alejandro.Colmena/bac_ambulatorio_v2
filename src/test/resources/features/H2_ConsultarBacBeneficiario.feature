#Paso2: Consulta de rut de beneficiario para Validar si posee BacActivo y solicitudes anteriores.
Feature: Consulta de Bac Beneficiario

  #Pendiente caso de beneficiario sin BAC pero con solicitudes anteriores.
  Scenario Outline: Validar Bac Activo
    Given Me encuentro en Consulta-Rut Beneficiario
    When Ingreso rut de beneficiario <rutBeneficiario>
    Then Valido si posee Bac activo <estadoBac>

    #Posee solicitudes anteriores BAC
    @FlujoCompleto @soloPaso2 @SoloBitacora
    Examples: 
      | rutBeneficiario | estadoBac |
      | "11629098-7"    | "Activo"  |

    @BenefSinBac
    Examples: 
      | rutBeneficiario | estadoBac  |
      | "111111-8"      | "Inactivo" |

  @FlujoCompleto 
  Scenario: Validar carga de datos de beneficiario
    Then Valido que se carge nombre y correo de beneficiario
	
	@FlujoCompleto
  Scenario: Validar datos de beneficiario no editables
    Then Valido que nombre y correo no sean editables

  @FlujoCompleto
  Scenario: Validar solicitudes anteriores
    Then Valido que se carguen las solicitudes anteriores del beneficiario

  @FlujoCompleto
  Scenario: Validar columnas detalle de solicitudes anteriores
    Then valido estructura de columnas de tabla de solicitudes anteriores

  
  Scenario: Validar que se pueda seguir editando solicitud anterior
    Given Me encuentro en solicitud anterior de beneficiario
    Then Valido si se puede seguir editando
