#Paso3: Ingreso de gasto o gastos, es posible no ingresar ninguno.
Feature: Ingreso de gastos

  @soloPaso2 @FlujoCompleto
  Scenario: Ingreso de gastos Sin descripcion
    Given Valido que me encuentro en Paso2
    When adjunto archivo de gasto sin descripción
    Then Valido que no se pueda agregar gasto sin descripción

  #Validar que no se pueda subir archivo de gasto con peso superior a 5mb.
  @soloPaso2 @FlujoCompleto
  Scenario: Subir archivo de gasto con peso permitido
    Given Existe texto en campo informacion
    When adjunto archivo de gasto con peso superior al permitido
    Then valido que no se pueda agregar archivo de gasto

  #Validar que no se pueda subir archivo formato WORD-EXCEL-XML
  @soloPaso2 @FlujoCompleto
  Scenario Outline: validar que no se puedan subir archivos en formato no permitidos
    Given Existe texto en campo información
    When adjunto archivo en formato no permitido <extension>
    Then valido que aparezca alerta en pantalla

    Examples: 
      | extension |
      | "word"    |
      | "excel"   |
      | "xml"     |

  #Validar que se puedan adjuntar archivos en formatos PDF-TXT-JPEG
  @soloPaso2 @FlujoCompleto
  Scenario Outline: Validar que se puedan subir solo archivos en formatos permitidos
    Given Existe texto en campo informacionArchivo
    When adjunto archivo en formato correcto <extension>
    Then valido que archivo se encuentra adjunto

    Examples: 
      | extension |
      | "pdf"     |
      | "txt"     |
      | "jpeg"    |

  #Validar que no se puedan adjuntar un mismo archivo 2 veces
  @soloPaso2 @FlujoCompleto
  Scenario: Validar que no se pueda adjuntar mismo archivo 2 veces
    Given Existe descripcion en campo descripcionDeGasto
    And no existe ningún archivo adjunto
    When adjunto un mismo archivo dos veces
    Then Valido que no se pueda adjuntar un mismo archivo

  #Validar que se pueda subir mas de un archivo permitido
  @soloPaso2 @FlujoCompleto
  Scenario: Validar subir mas de un archivo permitido
    Given Existe descripcion masDeUnArchivo en campo descripcion
    And no existe ningun archivo adjunto
    When adjunto mas de un archivo permitido
    Then valido que exista mas de un archivo en tabla

  #Validar que opcion de prestacion seleccionada correcponda a la indicada en tabla
  @soloPaso2 @FlujoCompleto
  Scenario Outline: Validar prestacion indicada
    Given Existe descripcion de prestacion en descripción
    And no existen ningun archivo adjunto
    When adjunto archivo indicando prestacion <tipoPrestacion>
    Then Valido que se indique en tabla prestacion indicada <tipoPrestacion>

    Examples: 
      | tipoPrestacion  |
      | "sinCobertura"  |
      | "noArancelada"  |
      | "otraCobertura" |
      
  #Validar eliminacion de documentos adjuntos
  @soloPaso2 @FlujoCompleto
  Scenario: Eliminar documentos adjuntos 
  Given Existe adjunto en tabla de gastos
  When Elimino archivo de gasto adjunto
  Then Valido que registro de elimino correctamente