Feature: Ingreso a BacAmbulatorio con perfiles Consultor y Creador.

  
  Scenario Outline: Ingreso Bac
    Given me encuentro en ruta BacAmbulatorio
    When ingreso usuario y contraseña <usuario><contraseña>
    Then Valido que se ingreso con exito con perfil <tipoUsuario>
    And Valido menus habilitados para usuario <tipoUsuario>

    @Consultor 
    Examples: 
      | tipoUsuario | usuario      | contraseña |
      | "consultor" | "15697628-8" | "prueba"   |

    @Creador @Flujo1 @Flujo2 @FlujoCompleto @soloPaso2 @SoloBitacora @BenefSinBac
    Examples: 
      | tipoUsuario | usuario      | contraseña |
      | "creador"   | "18006175-4" | "prueba"   |

    @Inexistente
    Examples: 
      | tipoUsuario   | usuario      | contraseña |
      | "inexistente" | "19999999-0" | "ABC123"   |
