Feature: Formulario de solicitud BAC 

  @FlujoCompleto
  Scenario: Verificar ID correlativo
    Given Me encuentro en Ingreso de formulario Paso1
    Then verifico si ID de solicitud se genero de forma correlativa

  Scenario Outline: Verificar descripcion de archivo adjunto
    Given Se cargue archivo en formato correcto <rutaArchivo>
    Then Valido que no se pueda adjuntar archivo a solicitud sin descripcion

    #Archivo debe poseer caracteristicas requeridas (peso menor a 5mb y en formato PDF,TXT o JPG)
    Examples: 
      | rutaArchivo                                                                                                    |
      | "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\DocumentoPdfOK.pdf" |

  Scenario Outline: Verificar peso maximo de archivo
    Given se cargue archivo con peso superior a maximo permitido <rutaArchivo>
    Then Valido que aparezca alerta en pantalla<pesoArchivo>

    #Peso Superior
    Examples: 
      | rutaArchivo                                                                                                          | pesoArchivo   |
      | "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\DocumentoPdfMayor5MB.pdf" | "superior5mb" |

    #Peso Inferior
    @FlujoCompleto
    Examples: 
      | rutaArchivo                                                                                                    | pesoArchivo   |
      | "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\DocumentoPdfOK.pdf" | "inferior5mb" |

  Scenario Outline: Validar formatos permitidos
    Given adjunto archivo de formulario solicitud <rutaArchivo>
    Then Valido que se adjunto archivo en formato correcto <rutaArchivo>
  
    Examples: 
      | rutaArchivo                                                                                                    |
      | "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\DocumentoWord.docx" |

    
    Examples: 
      | rutaArchivo                                                                                                     |
      | "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\DocumentoExcel.xlsx" |

    @FlujoCompleto
    Examples: 
      | rutaArchivo                                                                                                    |
      | "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\DocumentoPdfOK.pdf" |

    @FlujoCompleto
    Examples: 
      | rutaArchivo                                                                                                  |
      | "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\CoberturaBAC.jpg" |

    @FlujoCompleto
    Examples: 
      | rutaArchivo                                                                                                   |
      | "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\FormularioBac.txt" |

  Scenario Outline: Adjuntar mas de un archivo
    Given Exista cargado un formulario de solicitud
    Then Validar que no se pueda adjuntar mas de un formulario<formulario>

    Examples: 
      | formulario                                                                                                     |
      | "C:\\Users\\Jesús Galvez\\Desktop\\BAC Ambulatorio V2\\BAC Ambulatorio V2\\documentosTest\\DocumentoPdfOK.pdf" |

  @FlujoCompleto
  Scenario: Eliminar formulario adjunto
    Given Exsta cargado un formulario de solicitud
    When Intento eliminar formulario
    Then Valido que forulario se elimino correctamente
