#Validar que se pueda eliminar bitacora
Feature: IngresoBitácora Paso4

  @SoloBitacora @FlujoCompleto
  Scenario: Validara primera bitacora
    Given me encuentro en sección bitácora
    Then valido que exista primera bitácora creada

  @SoloBitacora @FlujoCompleto
  Scenario: Validar que no se cree bitacora sin glosa
    When agrego bitácora nueva sin glosa
    Then valido que exista mensaje de alerta

  @SoloBitacora @FlujoCompleto
  Scenario: Validar creación de bitacora con datos correctos
    Given boton agregar bitacora esta habilitado
    When agrego bitacora en formato correcto
    Then valido que se agrego bitacora correctamente

  @SoloBitacora @FlujoCompleto
  Scenario Outline: Controlar error glosa extensa
    Given me encuentro en ingreso de glosa bitacora
    When ingreso bitacora con glosa Extensa <glosaExtensa>
    Then valido que ingreso se realice sin problemas

    Examples: 
      | glosaExtensa                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
      | "Una de las principales modificaciones incorporadas en la legislación laboral, a principios de la década de los ’80, se refiere al manejo de las cotizaciones de salud, las que hasta esa fecha eran administradas únicamente por el Estado. A partir de ese momento, cada chileno tiene la libertad de elegir y decidir sobre la forma de utilizar sus cotizaciones destinadas a salud.Una de las principales modificaciones incorporadas en la legislación laboral, a principios de la década de los ’80, se refiere al manejo de las cotizaciones de salud, las que hasta esa fecha eran administradas únicamente por el Estado. A partir de ese momento, cada chileno tiene la libertad de elegir y decidir sobre la forma de utilizar sus cotizaciones destinadas a salud.Una de las principales modificaciones incorporadas en la legislación laboral, a principios de la década de los ’80, se refiere al manejo de las cotizaciones de salud, las que hasta esa fecha eran administradas únicamente por el Estado. A partir de ese momento, cada chileno tiene la libertad de elegir y decidir sobre la forma de utilizar sus cotizaciones destinadas a salud.Una de las principales modificaciones incorporadas en la legislación laboral, a principios de la década de los ’80, se refiere al manejo de las cotizaciones de salud, las que hasta esa fecha eran administradas únicamente por el Estado. A partir de ese momento, cada chileno tiene la libertad de elegir y decidir sobre la forma de utilizar sus cotizaciones destinadas a salud." |

     