public String getWeather(String rut) throws MalformedURLException,
	IOException {
	 
	//Code to make a webservice HTTP request
	String responseString = "";
	String outputString = "";
	String wsURL = "http://servicios.vidasecurity.cl/clientes/escritoriodigital.asmx";
	URL url = new URL(wsURL);
	URLConnection connection = url.openConnection();
	HttpURLConnection httpConn = (HttpURLConnection)connection;
	ByteArrayOutputStream bout = new ByteArrayOutputStream();
	String xmlInput =
	" <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">\n" +
	" <soapenv:Header/>\n" +
	" <soapenv:Body>\n" +
	"  <tem:ObtenerPersistencia>\n" +
	" <!--Optional:-->\n" +
	" <tem:rutusuario>"+rut+"</tem:rutusuario>\n" +
	"  <tem:usuario>IDC</tem:usuario>\n" +
	"  <tem:password>Security2017</tem:password>\n" +
	" <tem:ip>10.240.45.150</tem:ip>\n" +
	" <tem:useros>jhotha</tem:useros>\n" +
	"  </tem:ObtenerPersistencia>\n" +
	" </soapenv:Body>\n" +
	" </soapenv:Envelope>";
	
	 
	byte[] buffer = new byte[xmlInput.length()];
	buffer = xmlInput.getBytes();
	bout.write(buffer);
	byte[] b = bout.toByteArray();
	String SOAPAction =
	"http://tempuri.org/ObtenerPersistencia";
	// Set the appropriate HTTP parameters.
	httpConn.setRequestProperty("Content-Length",
	String.valueOf(b.length));
	httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
	httpConn.setRequestProperty("SOAPAction", SOAPAction);
	httpConn.setRequestMethod("POST");
	httpConn.setDoOutput(true);
	httpConn.setDoInput(true);
	OutputStream out = httpConn.getOutputStream();
	//Write the content of the request to the outputstream of the HTTP Connection.
	out.write(b);
	out.close();
	//Ready with sending the request.
	 
	//Read the response.
	InputStreamReader isr =
	new InputStreamReader(httpConn.getInputStream());
	BufferedReader in = new BufferedReader(isr);
	 
	//Write the SOAP message response to a String.
	while ((responseString = in.readLine()) != null) {
	outputString = outputString + responseString;
	}
	//Parse the String output to a org.w3c.dom.Document and be able to reach every node with the org.w3c.dom API.
	Document document = parseXmlFile(outputString);
	NodeList nodeLst = document.getElementsByTagName("Mensaje");
	String weatherResult = nodeLst.item(0).getTextContent();
	System.out.println("mensaje de acceso: " + weatherResult);
	 
	//Write the SOAP message formatted to the console.
	String formattedSOAPResponse = formatXML(outputString);
	//System.out.println(formattedSOAPResponse);
	return weatherResult;
	}